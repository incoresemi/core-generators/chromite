 git status
 pip3 install -r requirements.txt
 export PATH="/incspike/bin:$PATH"
 python3 -m configure.main -ispec sample_config/rv64imacshu/isa.yaml \
	 -customspec sample_config/rv64imacshu/custom.yaml \
	 -cspec sample_config/rv64imacshu/core.yaml \
	 -gspec sample_config/rv64imacshu/csr_grouping.yaml \
	 -dspec sample_config/rv64imacshu/debug.yaml --clean
 python3 -m configure.main -ispec sample_config/rv64imacshu/isa.yaml \
	 -customspec sample_config/rv64imacshu/custom.yaml \
	 -cspec sample_config/rv64imacshu/core.yaml \
	 -gspec sample_config/rv64imacshu/csr_grouping.yaml \
	 -dspec sample_config/rv64imacshu/debug.yaml
 make -j$(nproc) generate_verilog; make generate_boot_files
 cd ci
 pip3 install -r requirements.txt
 git clone https://github.com/incoresemi/river_core_plugins.git
 git clone https://gitlab.com/incoresemi/riscof-plugins.git
 river_core generate -c rv64imacshu/river-config-riscof.ini 
 river_core compile -c rv64imacshu/river-config-riscof.ini -t mywork/test_list.yaml 
