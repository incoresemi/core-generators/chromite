//See LICENSE.iitm for license details. See LICENSE.incore for more details.
/*

Author(s) : 
  - Neel Gala <neelgala@gmail.com>
  - Vipul
  - S Pawan Kumar <pawan.kumar@incoresemi.com> gitlab: @pawks github: @pawks
Details:
This module will interact with the memory subsystem to fetch relevant instructions from memory. The
module will also receive flushes from the write - back stage which could be because of a fence or trap
handling.
--------------------------------------------------------------------------------------------------
*/
package stage1;
`ifdef async_reset
  import RegOverrides  :: *;
`endif
  // -- package imports --//
  import FIFOF::*;
  import SpecialFIFOs::*;
  import CustomFIFOs::*;
  import FIFO::*;
  import GetPut::*;
  import Assert::*;
  import Vector :: *;

  // -- project imports --//
	import TxRx	          :: * ;            // for interstage buffer connection
  import ccore_types    :: * ;     // for pipe - line types
  import icache_types   :: * ;          // for global interface definitions
  import pipe_ifcs      :: * ;
`ifdef compressed
  import decompress     :: * ;
`endif
  `include "ccore_params.defines"// for core parameters
  `include "Logger.bsv"       // for logging display statements.

  `include "icache.defines" // for getting the irespwidth parameter

  // Enum to define the action to be taken when an instruction arrives.
  typedef enum {CheckPrev, None} ActionType deriving(Bits, Eq, FShow);

  typedef struct{
    Bit#(`vaddr) pc;
    Bit#(16) instruction;
    Bit#(2) epochs;
  `ifdef bpu
    BTBResponse btbresponse;
  `endif
  } PrevMeta deriving(Eq, Bits, FShow);

	interface Ifc_stage1;
    interface Ifc_s1_rx rx;
    interface Ifc_s1_tx tx;
    interface Ifc_s1_icache icache;
    interface Ifc_s1_common common;
	endinterface:Ifc_stage1

`ifdef stage1_noinline
  (*synthesize*)
`endif
  module mkstage1#(parameter Bit#(`xlen) hartid) (Ifc_stage1);

    String stage1=""; // defined for logger

    // --------------------- Start instantiations ------------------------//

    // The following registers are use to the maintain epochs from various pipeline stages:
    // writeback and execute stage.
    Reg#(Bit#(1)) rg_wEpoch <- mkRegA(0);
    Reg#(Bit#(1)) rg_eEpoch <- mkRegA(0);
    /*doc:reg: This register stores the epoch under which the buffer was written i.e the epoch in
     * which the buffer contents are valid.*/
    Reg#(Bit#(2)) rg_bEpoch <- mkRegA(0);

    /*doc:reg: This register stores the address prefix of the line in the buffer.*/
    Reg#(Bit#(TSub#(`vaddr,TLog#(TDiv#(`irespwidth,8))))) rg_bprefix <- mkRegA(0);

    /*doc:reg: These registers buffer the response of the cacheline and store it until the next
     * response is received. The valid register indicates which bytes of the buffer are valid words.*/
    Vector#(TDiv#(`irespwidth,32),Reg#(Bit#(32))) v_rg_buffer <- replicateM(mkRegA(0));
    /* Vector#(TDiv#(`irespwidth,32),Reg#(Bit#(1))) v_rg_valid <- replicateM(mkRegA(0)); */

  `ifdef compressed
    /* // This register indicates that the stage is running in a compressed mode i.e the new PC in
     * stage 1 is `pc from stage0 + 2` . This register is set when a compressed instruction is seen
     * for the first time i.e while running in 4 byte aligned mode or when the PC received from
     * stage0 is 2 byte aligned(to account for jumps to a 2 byte aligned location). This register is
     * cleared when an instruction packet from stage0 is dropped due to a flush or when a compressed
     * instruciton is seen in compressed mode.
     * This register is a CReg because when there is a flush from the pipeline and
     * overlap_redirections is enabled, then this register needs to be reset to False immediately.
     * When overlap_redirections is not enabled, the epochs mismatch will anyways cause this
     * register to be reset to False again.
  */
    Reg#(Bit#(1)) rg_compressed[3] <- mkCRegA(3,0);
  `endif

    Bool lv_is_c = `ifdef compressed True `else False `endif ;

    // This FIFO receives the response from the memory subsytem (a.k.a cache)
    FIFOF#(IMem_core_response#(`irespwidth, `iesize `ifdef hypervisor ,`paddr `endif )) ff_memory_response <- 
                                                        mkCustomSizedBypassFIFOF(`isb_cachebuffer);

    // This FIFO receives the response from the branch prediction unit (bpu or ras)
    RX#(Stage0PC#(`vaddr,`iesize)) rx_fromstage0 <- mkRX;

    /*doc:wire: This wire indicates that a memory response can be dequeued in this cycle.*/
    Wire#(Bool) wr_deq_mem <- mkWire();
    /*doc:wire: This wire indicates whether the value stored in the wr_mem_response is a valid
     * response from the cache.*/
    Wire#(Bool) wr_mem_response_valid <- mkDWire(False);
    /*doc:wire: This wire stores the value of the first response in the response Fifo from the
     * memory subsystem.*/
    Wire#(IMem_core_response#(TMul#(TMul#(`iblocks,`iwords),8),`iesize `ifdef hypervisor ,`paddr `endif )) wr_mem_response <- mkDWire(unpack(0));

    // FIFO to interface with the next pipeline stage
		TX#(PIPE1) tx_tostage2 <- mkTX;

  `ifdef rtldump
		TX#(CommitLogPacket) tx_commitlog <- mkTX;
  `endif

    // This variable holds the current epoch values of the pipe
    let curr_epoch = {rg_eEpoch, rg_wEpoch};

  `ifdef triggers
    Vector#(`trigger_num, Wire#(TriggerData)) v_trigger_data1 <- replicateM(mkWire());
    Vector#(`trigger_num, Wire#(Bit#(`xlen))) v_trigger_data2 <- replicateM(mkWire());
    Vector#(`trigger_num, Wire#(Bool)) v_trigger_enable <- replicateM(mkWire());
  `endif

    // ---------------------- End Instatiations --------------------------//


    // ---------------------- Start local function definitions ----------------//

    // this function will deque the response from i - mem fifo and the branch prediction fifo
    function Action deq_response = action
      wr_deq_mem <= True;
      rx_fromstage0.u.deq;
    endaction;

  `ifdef triggers

    function ActionValue#(Tuple2#(Bool, Bit#(`causesize))) check_trigger (Bit#(`vaddr) pc,
                           Bit#(32) instr `ifdef compressed , Bool compressed `endif ) = actionvalue
      Bool trap = False;
      Bit#(`causesize) cause = `Breakpoint;
      Bit#(`xlen) compare_value ;
      Bool chain = False;
      for(Integer i=0; i < `trigger_num; i=i+1)begin
        `logLevel( stage1, 3, $format("[%2d]STAGE1: Trigger[%2d] Data1: ",hartid, i, 
                                                                        fshow(v_trigger_data1[i])))
        `logLevel( stage1, 3, $format("[%2d]STAGE1: Trigger[%2d] Data2: ",hartid, i, 
                                                                        fshow(v_trigger_data2[i])))
        `logLevel( stage1, 3, $format("[%2d]STAGE1: Trigger[%2d] Enable: ",hartid, i, 
                                                                        fshow(v_trigger_enable[i])))
        if(v_trigger_enable[i] &&& v_trigger_data1[i] matches tagged MCONTROL .mc &&&
                              ((!trap && !chain) || (chain && trap)) &&& mc.execute == 1)begin
          Bit#(`xlen) trigger_compare = `ifdef compressed
                     (compressed && mc.size == 2)? zeroExtend(v_trigger_data2[i][15:0]): `endif
                                                   v_trigger_data2[i];
          if(mc.select == 0)
            compare_value = pc;
          else
            compare_value = zeroExtend(instr);

          if(mc.matched == 0)begin
            if(trigger_compare == compare_value)
              trap = True;
            else if(chain)
              trap = False;
          end
          if(mc.matched == 2)begin
            if(compare_value >= trigger_compare)
              trap = True;
            else if(chain)
              trap = False;
          end
          if(mc.matched == 3)begin
            if(compare_value < trigger_compare)
              trap = True;
            else if(chain)
              trap = False;
          end

        `ifdef debug
          if(trap && mc.action_ == 1)begin
            cause = `HaltTrigger;
            cause[`causesize - 1] = 1;
          end
        `endif
          chain = unpack(mc.chain);
        end
      end
      return tuple2(trap, cause);
    endactionvalue;
  `endif

    function Bit#(32) fn_extract_word(Vector#(TDiv#(`irespwidth,32), Bit#(32)) line,
                                                            Bit#(TLog#(TDiv#(`irespwidth,8))) idx);
      let _words = zip(shiftInAtN(line,0), line);
      Bit#(TSub#(TLog#(TDiv#(`irespwidth,8)),2)) lv_widx = truncateLSB(idx); 
      Bit#(2) _windex = truncate(idx);
      let _word = {tpl_1(_words[lv_widx]),tpl_2(_words[lv_widx])};
      case (_windex) matches
        0: return _word[31:0];
        2: return _word[47:16];
        default: return 0;
      endcase
    endfunction

    rule rl_load_mem_resp;
      wr_mem_response <= ff_memory_response.first;
      wr_mem_response_valid <= ff_memory_response.notEmpty();
    endrule

    rule rl_deq_mem_resp(wr_deq_mem && ff_memory_response.notEmpty()); 
      let lv_pkt = ff_memory_response.first;
      `logLevel( stage1, 1,$format("[%2d]STAGE1 : Dequeueing ICACHE response.",
          hartid))
      ff_memory_response.deq;
    endrule

    // ---------------------- End local function definitions ------------------//

    /*doc:rule: This rule processes the instructions based on pc from stage0. If running in
     compressed mode the new PC is rounded up to be 2 byte aligned. Then the instruction is
     extracted from the buffer or icache response (depending on indication from stage0). The case
     where a 4 byte instruction straddles cacheline boundary, the cache response is not dequeued but
     the data is used to construct the instruction. Hence this rule will not fire until the response
     from the cache for the next line is available. Incase its a straddle case but the next
     instruction need not be executed as it is a redirection to another location, the discard field
     is set to true in the packet. Hence both the stage0 pkt and the icache response is dropped.

     The rg_compressed register is used to detect if the execution context is running in the
     "compressed" mode i.e the pc from stage0 needs to be rounded up to the nearest 2 byte
     alignment. It is set when a compressed instruction is seen in the instruction flow in normal
     mode or the pc from stage0 is 2 byte aligned. It is cleared when a compressed instruction is
     seen in compressed mode or when a flush takes place(this does not hamper redirections to 2 byte
     locations as the redirect pc would have the actual value and is passed as is to this stage
     before being updated to the nearest 4 byte alignment in stage0).
    
     The btb response is reset based on whether the instruction is compressed or if it is an
     uncompressed instruction at a 2 byte boundary.
    */
    rule rl_process_instruction(rx_fromstage0.u.notEmpty && tx_tostage2.u.notFull);
      let stage0pc = rx_fromstage0.u.first;
      // Construct PC by setting pc[1] = 1 if running in compressed mode
      let pc = stage0pc.address  `ifdef compressed | zeroExtend({rg_compressed[0],1'b0}) `endif ;
    /* `ifdef compressed */
      Bit#(TSub#(`vaddr,1)) lv_pc = truncateLSB(pc);
      Bit#(TSub#(TLog#(TDiv#(`respwidth,8)),1)) lv_line_offset = truncate(lv_pc);
      Bool edgecase = unpack(&lv_line_offset) && lv_is_c;
    /* `endif */
    `ifdef bpu
      let btbresponse = stage0pc.btbresponse;
    `endif
      // capture the response from the cache
      let imem_resp = wr_mem_response;
      Vector#(TDiv#(`irespwidth,32),Bit#(32)) lv_frm_ic = unpack(imem_resp.line);
      // Read buffer contents
      let lv_buff = readVReg(v_rg_buffer);
      // Extract word from cache response based on LSB of pc
      let lv_ic_word = fn_extract_word(lv_frm_ic, truncate(pc));
      // Extract word from buffer based on LSB of pc
      let lv_buff_word = fn_extract_word(lv_buff, truncate(pc));
      let lv_deq = (rg_bprefix == truncateLSB(pc));
      let lv_service_edgecase = rg_bEpoch == stage0pc.epochs && lv_deq && lv_is_c ;
      // Variable indicating that the instruction word should be picked from the cache response
      let lv_pick_cache = stage0pc.cache && !(lv_service_edgecase) ;
      let lv_inst_word = (lv_pick_cache) ? lv_ic_word : lv_buff_word;
      `ifdef compressed
        let lv_compressed = rg_compressed[0];
      `endif
      // Detect memory subsystem race condition
      let lv_skewed_mem_resp = (stage0pc.cache 
          && imem_resp.epochs != stage0pc.epochs && wr_mem_response_valid 
          `ifdef compressed && !unpack(lv_compressed) `endif );
      // Drop instruction if the epoch in stage0 pkt is obsolete or stage0 indicates that it needs
      // to be discarded.
      let lv_drop_inst = curr_epoch != stage0pc.epochs `ifdef compressed || (stage0pc.discard) `endif ;
      let lv_deq_mem = False;
      Bool lv_wait = ((lv_pick_cache || edgecase) && !wr_mem_response_valid);
      Bool trap = imem_resp.trap && 
        (stage0pc.cache `ifdef compressed || edgecase`endif );


    `ifdef bpu
      // Detect if the current instruction is causing a redirection. This helps in resetting the
      // compressed mode and returning to normal execution if the redirection target is 4byte
      // aligned      
      `ifdef compressed
      Bool lv_redirect =  unpack(~(pack(btbresponse.hi) ^ pack(pc[1]))) &&
                          btbresponse.btbhit && btbresponse.prediction > 1 ;
      `else
      Bool lv_redirect = False;
      `endif
    `endif
      `logLevel( stage1, 1, $format("[%2d]STAGE1 : ",hartid, fshow(stage0pc)))
    `ifdef compressed
      `logLevel( stage1, 0, $format("[%2d]STAGE1 : Straddle:%b rg_compressed:%b lv_wait:%b ",
          hartid, edgecase, rg_compressed[0], lv_wait))
    `endif
      /*for(Integer i = 0; i<valueof(TDiv#(`irespwidth,32)); i=i+4)
        `logLevel( stage1, 2, $format("[%2d]STAGE1 : Buffer[%4d]:%h Buffer[%4d]:%h Buffer[%4d]:%h Buffer[%4d]:%h",
          hartid, i , v_rg_buffer[i], i+1, v_rg_buffer[i+1], i+2, v_rg_buffer[i+2], i+3, v_rg_buffer[i+3]))*/
      `logLevel( stage1, 2, $format("[%2d]STAGE1 : bepoch:'b%b bprefix:'h%h lv_deq:'b%b",hartid,rg_bEpoch, rg_bprefix, lv_deq))

      // local variable to hold the instruction to be enqueued
      Bit#(32) final_instruction=?;

      // local variable to indicate if the instruction being analysed is compressed or not
      Bool compressed = False;

      // local variable indicating if the current instruction under analysis should be enqueued to
      // the next stage or dropped.
      Bool enque_instruction = True;
      `ifdef compressed
      // Force instruction to contain uppermost 2 bytes of buffer as LSB and lowermost 2 bytes of
      // cache response as MSB in the event of an edgecase.
      if(edgecase && lv_service_edgecase) begin
        let prev_word = last(lv_buff)[31:16];
        lv_inst_word ={imem_resp.line[15 : 0], prev_word};
      end
      if(!lv_service_edgecase && edgecase) begin
        lv_deq_mem = stage0pc.cache;
      end
      `endif
      // Wait for response from the cache
      if(!lv_wait) begin
      // if epochs do not match then drop the instruction
      if(lv_drop_inst || lv_skewed_mem_resp)begin
        // Don't deq stage0 pkt if its only a memory race condition
        if(lv_drop_inst) begin
          lv_deq_mem = `ifdef compressed ( `endif stage0pc.cache `ifdef compressed ||
                                                                            stage0pc.discard) `endif 
                                      && imem_resp.epochs == stage0pc.epochs;
          rx_fromstage0.u.deq;
          `logLevel( stage1, 1,$format("[%2d]STAGE1 : Dropping Instruction. ExpEpoch:%b CurrEpoch:%b",
            hartid, stage0pc.epochs, curr_epoch))
          `ifdef compressed
          lv_compressed = 0;
          `endif
        end
        else begin
         lv_deq_mem = True;
        `logLevel(stage1, 1,$format("[%2d]STAGE1 : Dropping Memory response. ExpEpoch:%b MemEpoch:%b",
            hartid, stage0pc.epochs, imem_resp.epochs))
        end

        /* deq_response; */
        enque_instruction = False;
      end
      else begin
        // 4 byte instruction
        if(lv_inst_word[1 : 0] == 'b11) begin
         /* deq_response; */
         // if only the lower 2 bytes are available and its an edgecase scenario, do not enqueue
         // instruction and wait for another cache response. This particular if case is usefull when
         // 2 cache responses are necessary to obtain 1 instruction(example case redirection to a 4
         // byte instruction on a 2 byte boundary and response boundary)
          if(edgecase && !lv_service_edgecase) begin
            enque_instruction = False;
          end
          else begin
            lv_deq_mem = !edgecase && !lv_service_edgecase && (stage0pc.cache) ;
            rx_fromstage0.u.deq;
            final_instruction = lv_inst_word;
          end
        end
        `ifdef compressed
        // 2 byte instruction
        else begin
          compressed = True;
          final_instruction = zeroExtend(lv_inst_word[15:0]);
          lv_deq_mem = stage0pc.cache && !lv_service_edgecase;
          if(pc[1] == 1 `ifdef bpu || lv_redirect `endif ) begin
            rx_fromstage0.u.deq;
            /* deq_response; */
          end
          // Don't trap due to imem resp if a 2 byte compressed instruction can be issued in this cycle
          if(trap && imem_resp.trap && lv_service_edgecase) begin
            trap = False;
          end
            /* lv_deq_mem = True; */
        end
        `ifdef bpu
        // reset prediction if it wasn't for this instruction
        if(!lv_redirect && btbresponse.prediction>1 ) begin
          btbresponse.btbhit = False;
          btbresponse.prediction = 1;
        end
        `endif
        `endif
      end
      Bit#(`causesize) cause = imem_resp.cause;
    `ifdef hypervisor 
      Bit#(`paddr) gpa = imem_resp.gpa;
    `endif
    `ifdef triggers
      let {trig_trap, trig_cause} <- check_trigger(pc, final_instruction
                                        `ifdef compressed ,compressed `endif );
      if(trig_trap)begin
        trap = True;
        cause = trig_cause;
      end
    `endif
      Bit#(32) inst = final_instruction;
    `ifdef compressed
      if(compressed)
        final_instruction = fn_decompress(truncate(final_instruction));
    `endif
			let pipedata = PIPE1{program_counter : pc,
                    instruction : final_instruction,
                    epochs:{rg_eEpoch, rg_wEpoch},
                    trap : trap
                  `ifdef bpu
                    ,btbresponse: btbresponse
                  `endif
                  `ifdef compressed
                    ,upper_err : imem_resp.trap && edgecase && !compressed
                    ,compressed: compressed
                  `endif
                    ,cause : cause
                  `ifdef hypervisor 
                    ,gpa : gpa
                  `endif };
      `logLevel( stage1, 0, $format("[%2d]STAGE1 : PC:%h inst:%h compressed:%b epochs:%b trap:%b",hartid,pc,lv_inst_word, compressed,{rg_eEpoch, rg_wEpoch}, trap))
      `logLevel( stage1, 2, $format("[%2d]STAGE1 : mem_resp_valid:%b, ",hartid,wr_mem_response_valid, fshow(wr_mem_response)))
      if(enque_instruction) begin
        `ifdef compressed
        lv_compressed = (pc[1] ^ pack(compressed)) `ifdef bpu & ~(pack(lv_redirect) `endif );
        `endif
      `ifdef rtldump
        tx_commitlog.u.enq(CommitLogPacket{instruction: inst, pc: pc, mode: ?,
            inst_type: tagged None});
      `endif
        tx_tostage2.u.enq(pipedata);
        `logLevel( stage1, 0,$format("[%2d]STAGE1 : Sending packet to Stage2 ",hartid))
      end
      `ifdef compressed
      rg_compressed[0] <= lv_compressed;
      `endif
      end
      if(!lv_wait && lv_deq_mem && !lv_skewed_mem_resp && !lv_drop_inst) begin
        for(Integer i = 0; i<valueof(TDiv#(`irespwidth,32)); i=i+1)
          v_rg_buffer[i] <= lv_frm_ic[i];
          rg_bEpoch <= imem_resp.epochs;
          rg_bprefix <= truncateLSB(pc);
      end
      wr_deq_mem <= lv_deq_mem;
    endrule:rl_process_instruction

    // Description : This method will capture the response from the memory subsytem and enque it in
    // a FIFO. One could of think of performing all the function in the process_instruction
    // rule in this method itself. This would only work if you are not supporting compressed
    // instructions. When you support compressed, the cache can send a single response which
    // contains 2 16 - bit instruction. In such a case the process_instruction rule will fire twice
    // and deque the fifo only on the second run. Thus we need to have a fifo which will store the
    // response from the cache for an extra cycle.
    // The former approach could work with compressed as well if : we process both the instructions
    // and enqueue them simultaneously into the next stage. Not sure what other dependencies would
    // be there?
    interface icache = interface Ifc_s1_icache
  		interface inst_response = interface Put
  			method Action put (IMem_core_response#(`irespwidth, `iesize `ifdef hypervisor ,`paddr `endif ) resp) 
  			  if (ff_memory_response.notFull);
          ff_memory_response.enq(resp);
  			endmethod
      endinterface;
    endinterface;

    // Description : This interface will capture the prediction response from the BTB module. If
    // compressed is supported the, BTB will provide 2 predictions for each of the 2byte addresses
    // that have been fetched from the I - mem. If compressed is not supported then a single
    // prediction is only provided for the entire 32 - bit instruction has been received from the
    // I - cache.
    interface rx = interface Ifc_s1_rx
      interface rx_from_stage0 = rx_fromstage0.e;
    endinterface;

    // Description : This method will transmit the instruction to the next stage.
    interface tx = interface Ifc_s1_tx
  		interface tx_to_stage2 = tx_tostage2.e;
    `ifdef rtldump
	  	interface tx_commitlog = tx_commitlog.e;
    `endif
    endinterface;

    interface common = interface Ifc_s1_common
      method Action ma_update_eEpoch;
        rg_eEpoch<=~rg_eEpoch;
        `ifdef compressed
        rg_compressed[1] <= 0;
        `endif
      endmethod
  
      method Action ma_update_wEpoch;
        rg_wEpoch<=~rg_wEpoch;
        `ifdef compressed
        rg_compressed[2] <= 0;
        `endif
      endmethod
  
    `ifdef triggers
      method Action trigger_data1(Vector#(`trigger_num, TriggerData) t);
        for(Integer i=0; i<`trigger_num; i=i+1)
          v_trigger_data1[i] <= t[i];
      endmethod
      method Action trigger_data2(Vector#(`trigger_num, Bit#(`xlen)) t);
        for(Integer i=0; i<`trigger_num; i=i+1)
          v_trigger_data2[i] <= t[i];
      endmethod
      method Action trigger_enable(Vector#(`trigger_num, Bool) t);
        for(Integer i=0; i<`trigger_num; i=i+1)
          v_trigger_enable[i] <= t[i];
      endmethod
    `endif
    endinterface;
  endmodule
endpackage

