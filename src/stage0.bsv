/* 
see LICENSE.incore
see LICENSE.iitm

Author(s): 
- Neel Gala <neelgala@gmail.com>
- S Pawan Kumar <pawan.kumar@incoresemi.com> gitlab: @pawks github: @pawks

*/
/*doc:overview:
This module implements the pc-gen functionality. It incorporates the branch predictor as
well. Based on the outputs of the branch predictor (if enabled), pc+4 and any flushes in the same
cycle, this module decides what the next pc to the cache and stage1 should be.

On Reset
^^^^^^^^
Once reset is de-asserted, the rg_pc register is assigned the value of the resetpc input. All other
functionality only takes action after this initialization done.

Handling Flush/Re-direction
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A flush signal is received in this module under one of the following conditions:

1. A fence instruction has reached the write-back stage (if icache or bpu are implemented).
2. A sfence instruction has reached the write-back stage (if supervisor is present)
3. A misprediction occurred in the execute-stage
4. A trap or csr operation has reached the write-back stage.

Under any of the above conditions, the register rg_pc is set to the new pc that needs to be fetched.

If a flush occurs due to a fence or sfence, then in the subsequent cycle the instruction memory
subsystem receives the fence/sfence operation while the branch predictor is not accessed. In the next
cycle, the new pc with fence/sfence disabled is then sent to both the branch predictor and the
instruction memory subsystem.

Branch Prediction
^^^^^^^^^^^^^^^^^

Currently it is expected for the branch predictor to respond with the prediction in the same cycle.

If compressed is supported, the working is slightly more complex. The predictor is expected to
respond with predictions for both ``pc`` and ``pc+2``. There is also a special ``edgecase`` which
needs to be handled and has explained in detail in the description of the rule: ``rl_gen_next_pc``.

Redirection Latency
^^^^^^^^^^^^^^^^^^^

The wr_port and rd_port macros below indicate if the cache is presented with a redirected PC in the
same cycle of the next cycle. In order to reduce the misprediction penalty it is best to latch the
new PC to the icache in the same cycle the branch resolution happend. We can achieve this by
treating the rg_pc register as a CReg where the writes preceed the reads in the same cycle thus
acting like a bypass register. Similary, rg_sfence, rg_fence, rg_delayed_redirect and the epochs
will need to have the same mechanism to maintain proper synchronization. 

Basically, if wr_port < rd_port then bypass is enabled, else redirection latency is increased by
one more cycle.
*/

`ifdef overlap_redirections
  `define wr_port 0
  `define rd_port 1
`else
  `define wr_port 1
  `define rd_port 0
`endif
package stage0;

`ifdef async_reset
  import RegOverrides  :: *;
`endif
  // -- library imports
  import FIFO           :: * ;
  import FIFOF          :: * ;
  import SpecialFIFOs   :: * ;
  import GetPut         :: * ;
  import TxRx           :: * ;
  import DefaultValue   :: * ;

  // -- project imports
  `include "Logger.bsv"
  `include "ccore_params.defines"
  `include "icache.defines"
  import icache_types   :: * ;
  import pipe_ifcs      :: * ;
  import ccore_types :: * ;
`ifdef bpu
  import gshare_fa :: * ;
`endif
`ifdef supervisor
  import mmu_types      :: * ;
`endif

  interface Ifc_stage0;
    interface Ifc_s0_common common;
    interface Ifc_s0_icache icache;
    interface Ifc_s0_tx tx;
  `ifdef bpu
    interface Ifc_s0_bpu s0_bpu;
  `endif
  endinterface: Ifc_stage0

`ifdef stage0_noinline
  (*synthesize*)
`endif
  module mkstage0#(Bit#(`vaddr) resetpc, parameter Bit#(`xlen) hartid) (Ifc_stage0);
    String stage0 = "";
  `ifdef bpu
    Ifc_bpu bpu <- mkbpu(hartid);
  `endif

    /*doc:fifo: This fifo holds the request to be sent to the I-cache/I-Mem subsystem. This is a
    * bypass fifo because we expect the i-cache to have a LFIFO/SizedFIFO at its end*/
    FIFOF#(IMem_core_request#(`vaddr, `iesize)) ff_to_cache <- mkBypassFIFOF;

    // holds the info to be send to the stage1
    TX#(Stage0PC#(`vaddr,`iesize)) tx_tostage1 <- mkTX;

    /*doc:reg: holds the program counter*/
    Reg#(Bit#(`vaddr)) rg_pc[2] <- mkCRegA(2, 'h1000);

    /*doc:reg: register to maintain the epoch in sync with execute stage*/
    Reg#(Bit#(1)) rg_eEpoch[2] <- mkCRegA(2,0);

    /*doc:reg: register to maintain the epoch in sync with execute stage*/
    Reg#(Bit#(1)) rg_wEpoch[2] <- mkCRegA(2,0);

    /*doc:reg: Register to maintain the epoch in which the last cache request was sent.*/
    Reg#(Bit#(2)) rg_bEpoch <- mkRegA(0);

    /*doc:reg: This register is used in the initializing the pc with reset-pc being driven by SoC.*/
    Reg#(Bool) rg_initialize <- mkRegA(True);

    /*doc:reg: This register is used to store the prefix of the previous address sent to the cache.*/  
    Reg#(Bit#(TSub#(`vaddr,TLog#(TDiv#(`irespwidth,8))))) rg_prev_prefix <- mkRegA(0);

    /*doc:wire: captures the condition when the reset sequence is done*/
    Wire#(Bool) wr_reset_sequence_done <- mkWire();
    /*doc:wire: wire to hold the current privilege mode*/
    Wire#(Bit#(2)) wr_priv <- mkWire();
    /*doc:wire: wire to hold the current value of mstatus*/
    Wire#(Bit#(`xlen)) wr_mstatus <- mkWire();
  `ifdef supervisor  
    /*doc:wire: wire to hold the current value of satp*/
    Wire#(Bit#(`xlen)) wr_satp <- mkWire();
  `endif
  
  `ifdef hypervisor 
    Wire#(Bit#(1)) wr_virtual <- mkWire();
  `endif

  `ifdef ifence
    /*doc:reg: When true indicates that the flush occurred due to a fence*/
    Reg#(Bool) rg_fence[2] <- mkCRegA(2, False);
  `endif

  `ifdef supervisor
    /*doc:reg: When true indicates that the flush occurred due to a sfence*/
    Reg#(SfenceReq#(`xlen, `asidwidth)) rg_sfence[2] <- mkCRegA(2, defaultValue);
  `endif

`ifdef bpu
  `ifdef compressed
    /*doc:reg: This register when Valid indicates a 32-bit control instruction on a
    2-byte boundary straddling response boundary is predicted taken. 
    Thus the cache is sent the pc+4 address to ensure that the upper-16 bits are received before
    redirecting. This packet to the stage1 also has the discard bit set, indicating that the
    response needs to be discarded and not buffered, regardless of the presence of the redirection 
    target in the same line.*/
    Reg#(Maybe#(Bit#(`vaddr))) rg_delayed_redirect[2] <- mkCRegA(2, tagged Invalid);
  `endif
`endif

    // local variable to hold the next+4 pc value. Ensure only a single adder is used.
    let curr_epoch = {rg_eEpoch[`rd_port], rg_wEpoch[`rd_port]};

    /*doc:rule: This rule will fire only once immediately after reset is de-asserted. The rg_pc is
    initialized with the resetpc argument*/
    rule rl_initialize (rg_initialize && wr_reset_sequence_done);
      rg_initialize <= False;
      rg_pc[1] <= resetpc;
      `logLevel( stage0, 0, $format("STAGE0: Reset PC:%h",resetpc))
    endrule

    /*doc:rule: This rule muxes between pc+4 and the prediction provided by the bpu.
    When the rg_fence is True the request is sent only to the cache and not to stage1.
    The same is the case with rg_sfence. This is because the i-cache does not respond on a fence
    request to stage1. When either rg_fence or rg_sfence are set to True, the rg_pc is not
    updated. It re-used as a valid request in the next cycle by which the rg_fence and rg_sfence
    have been made false.

    The pc is sent to stage1 on all accounts, however the pc is sent to the cache only if the prefix
    of the request does not match or there has been a flush in the pipe. This ensures maximum use of
    the extracted data from the cache while lowering power consumption. In the event that there is a
    branch to a 2 byte aligned address which contains a 4 byte instruction and straddles across the
    response boundary, 2 requests are sent to the cache, first pc then pc+4 is sent to the cache.
    Then normal execution is resumed. Incase the instruction is a redirection, the pc+4 is marked to
    be discarded and stage1 ignores the pc+4 instruction.
    We refer to this scenario as an edge case and the register rg_delayed_redirect ensures correct
    pc sequences are sent to the cache and stage1
    */
    rule rl_gen_next_pc (tx_tostage1.u.notFull && !rg_initialize && wr_reset_sequence_done);
      `ifdef bpu
        PredictionResponse bpu_resp = ?;
      `endif

        let nextpc = (rg_pc[`rd_port] & ~3) + 4;
        `logLevel( stage0, 0, $format("[%2d]STAGE0: nextpc: %h ",hartid, nextpc `ifdef ifence ," fencei:%b",rg_fence[`rd_port] `endif ))

        Bit#(TSub#(`vaddr,2)) lv_pc = truncateLSB(rg_pc[`rd_port]);
        Bit#(TSub#(TLog#(TDiv#(`respwidth,8)),2)) lv_line_offset = truncate(lv_pc);

        Bool edgecase = unpack(&lv_line_offset);

      `ifdef bpu
        // bpu is flushed in case of ifence and not for sfence
        if( `ifdef supervisor !rg_sfence[`rd_port].sfence && `endif True) begin
          let bpuresp <- bpu.mav_prediction_response(PredictionRequest{pc: rg_pc[`rd_port]
                                    `ifdef ifence     ,fence: rg_fence[`rd_port] `endif
                                    `ifdef compressed , compressed: rg_pc[`rd_port][1]==1 `endif });
        `ifdef compressed
          // send new target from previously predicted edgecase Ci
          if(rg_delayed_redirect[`rd_port] matches tagged Valid .rpc) begin
            nextpc = rpc;
            rg_delayed_redirect[`rd_port] <= tagged Invalid;
          end
          // send pc+4 and store the target for the next round
          else if(edgecase && bpuresp.btbresponse.hi 
                                      && bpuresp.btbresponse.prediction > 1 && !rg_fence[`rd_port]
                              `ifdef supervisor && !rg_sfence[`rd_port].sfence `endif )
            rg_delayed_redirect[`rd_port] <= tagged Valid bpuresp.nextpc;
          else begin
        `endif
          if (bpuresp.btbresponse.prediction[`statesize - 1] == 1 && bpuresp.btbresponse.btbhit)
                                    /* `ifdef compressed && !edgecase `endif ) */
            nextpc = bpuresp.nextpc;
         `ifdef compressed
           end
          `endif

          bpu_resp = bpuresp;
          `logLevel( stage0, 0, $format("[%2d]STAGE0: BPU response:",hartid,fshow(bpu_resp)))
        end
      `endif

      // don't update the pc in case fence or sfence instruction
      if( `ifdef ifence !rg_fence[`rd_port] && `endif `ifdef supervisor !rg_sfence[`rd_port].sfence && `endif True) begin
        rg_pc[`rd_port] <= nextpc ;
        `logLevel( stage0, 0, $format("[%2d]STAGE0: nextpc assigned:%h ",hartid, nextpc))
      end

      `ifdef ifence
        if(rg_fence[`rd_port])
          rg_fence[`rd_port] <= False;
      `endif

      `ifdef supervisor
        if(rg_sfence[`rd_port].sfence)
          rg_sfence[`rd_port] <= SfenceReq{sfence: False};
      `endif

        Bit#(TSub#(`vaddr,TLog#(TDiv#(`irespwidth,8)))) lv_prefix = truncateLSB(rg_pc[`rd_port]);
        
        Bool lv_pref_mismatch = ((rg_prev_prefix != lv_prefix) || (rg_bEpoch != curr_epoch));
        // Enqueue request to icache only if its a different line or if there is a *fence op
        Bool lv_enq_icache = lv_pref_mismatch
                              `ifdef ifence || rg_fence[`rd_port] `endif 
                              `ifdef supervisor || rg_sfence[`rd_port].sfence `endif ;
          `logLevel(stage0, 0, $format("[%2d]STAGE0: Cur Prefix: %h, Last Prefix: %h \
Cur Epoch: %h Last Epoch: %h",hartid, lv_prefix,rg_prev_prefix, curr_epoch, rg_bEpoch))
        if(lv_enq_icache) begin
          `logLevel( stage0, 0, $format("[%2d]STAGE0: Sending PC:%h to I$. ",hartid, rg_pc[`rd_port]))
          Bit#(1) mxr  = wr_mstatus[19];
          Bit#(1) sum  = wr_mstatus[18];
          ff_to_cache.enq(IMem_core_request{address  : rg_pc[`rd_port],
                                        epochs  : curr_epoch
                                        ,priv   : wr_priv
                                        ,mxr    : mxr 
                                        ,sum    : sum 
                  `ifdef supervisor    ,satp   : wr_satp
                                       ,sfence_req  : rg_sfence[`rd_port] `endif
                  `ifdef hypervisor    ,v       : wr_virtual  `endif
                  `ifdef ifence        ,fence   : rg_fence[`rd_port]     `endif });

        end
        // Do not update prefix in case its the pc+4 of the edge case. This ensures that the
        // redirected pc is sent unconditionally i.e even if the redirect pc is in the same line as
        // pc+4
        if (lv_enq_icache `ifdef compressed && !isValid(rg_delayed_redirect[`rd_port]) `endif )
          rg_prev_prefix <= lv_prefix;
        if( `ifdef ifence !rg_fence[`rd_port] && `endif `ifdef supervisor !rg_sfence[`rd_port].sfence && `endif 
             lv_pref_mismatch )
          rg_bEpoch <= curr_epoch;

        if( `ifdef ifence !rg_fence[`rd_port] && `endif `ifdef supervisor !rg_sfence[`rd_port].sfence && `endif True) begin
          tx_tostage1.u.enq(Stage0PC{   address      : rg_pc[`rd_port] ,
                                        cache        : lv_enq_icache,
                                        epochs       : curr_epoch
                    `ifdef compressed   ,discard     : isValid(rg_delayed_redirect[`rd_port]) `endif
                    `ifdef bpu          ,btbresponse : bpu_resp.btbresponse `endif  });
          `logLevel( stage0, 0, $format("[%2d]STAGE0: Sending PC:%h to Stage1",hartid, rg_pc[`rd_port]))
        end
    endrule

    interface icache = interface Ifc_s0_icache
      interface to_icache = toGet(ff_to_cache);
    endinterface;

    interface tx = interface Ifc_s0_tx
      interface tx_to_stage1 = tx_tostage1.e;
    endinterface;

    interface common = interface Ifc_s0_common
      method Action ma_update_eEpoch ();
        rg_eEpoch[`wr_port] <= ~rg_eEpoch[`wr_port];
      endmethod
  
      method Action ma_update_wEpoch ();
        rg_wEpoch[`wr_port] <= ~rg_wEpoch[`wr_port];
      endmethod
      method Action ma_reset_done(Bool _done);
        wr_reset_sequence_done <= _done;
      endmethod:ma_reset_done

      method Action ma_flush (Stage0Flush fl) if(!rg_initialize && wr_reset_sequence_done);
        `logLevel( stage0, 1, $format("[%2d]STAGE0: Recieved Flush:",hartid,fshow(fl)))
      `ifdef ifence
        rg_fence[`wr_port] <= fl.fence;
      `endif
      `ifdef supervisor
        rg_sfence[`wr_port] <= fl.sfence_req;
      `endif
        rg_pc[`wr_port] <= fl.pc;
    `ifdef bpu
      `ifdef compressed
        // reset any delayed-redirect
        rg_delayed_redirect[`wr_port] <= tagged Invalid;
      `endif
    `endif
      endmethod

      /*doc:method: this method is added to sample the privilage*/
      method Action ma_priv (Bit#(2) p);
        wr_priv <= p;
      endmethod:ma_priv 
      /*doc:method: this method is added to sample the mstatus register*/
      method Action ma_mstatus(Bit#(`xlen) m);
        wr_mstatus <= m;
      endmethod:ma_mstatus
    `ifdef supervisor
      /*doc:method: this method is added to sample the satp*/
      method Action ma_satp (Bit#(`xlen) s);
        wr_satp <= s;
      endmethod:ma_satp 
    `endif

    `ifdef hypervisor 
      /*doc:method: this method is added to sample the virtulization V*/
      method Action ma_virtual (Bit#(1) v);
        wr_virtual <= v;
      endmethod:ma_virtual 
    `endif

    endinterface;

`ifdef bpu
    interface s0_bpu = interface Ifc_s0_bpu
      method ma_train_bpu   = bpu.ma_train_bpu;
    `ifdef gshare
      method ma_mispredict  = bpu.ma_mispredict;
    `endif
      method ma_bpu_enable  = bpu.ma_bpu_enable;
    endinterface;
`endif

  endmodule: mkstage0
endpackage: stage0

