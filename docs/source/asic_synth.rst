##############
ASIC Synthesis
##############

This section will describe how the core-pipeline RTL can be synhtesized using PDK. For now synthesis
scripts for the following tools are available : 

- Genus Synthesis (Cadence)


.. When synthesizing for an FPGA/ASIC, the top module should be ``mkccore_axi4 (mkccore_axi4.v)`` 
.. as the top module. 
.. 
.. The ``mkimem`` and ``mkdmem`` module include SRAM instances which implement the respective data 
.. and tag arrays. These are implemented as BRAMs and thus require no changes for FPGAs. 
.. However for an ASIC flow, it is strictly advised to replace the BRAMs with respective SRAMs. 

In order to synthesize the core-pipeline, before performing the steps in :numref:`build` one should
change the following variables of the ``bsc_compile_options`` node in ``sample_config/rv64imacsu/core.yaml`` to the following:

.. code-block:: yaml

   top_module : mkriscv
   top_dir    : src
   top_file   : riscv
   compile_target : asic
   trace_dump : false
   assertions : false

You can also change the asic parameters in the core.yaml accordingly:

.. code-block:: yaml

   asic_params:
     tech_size: 65
     frequency: 600
    

Once the build is done, you should see a file `build/chromite_genus_synth.tcl` generated. This file
will include all the parameters that will be required by the genus synthesis tool. To invoke
synthesis execute the following commands: 

.. code-block:: shell

   cd asic_synthesis/genus/run
   make synth user_tcl=../../../build/chromite_genus_synth.tcl pd_tcl=<path to pd input tcl without the .tcl extension>

The `pd_tcl` file will have to be provided by the user similar to template shown in
`asic_synthesis/genus/samples/example_pd.tcl`
