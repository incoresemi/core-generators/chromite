Page Table Walk (PTW) HYPERVISOR
=====================

<<TODO>> interface diagram.
<<TODO>> state-machine describing the working.
<<TODO>> two stage address translation 
<<TODO>> resending core request? hold_req buffer and its operation?
<<TODO>> PTwalk are cached and treated as machine accesses

.. figure:: tlbs-ptw.png
    :align: center

Hypervisor supports two-stage address translation, for any virtual memory access the original virtual address is converted in
the first stage byu VS-level address translation, as controlled by vsatp register, into guest physical
address. The guest physical address is then converted in the second stage by guest physical address
translation as controlled by the hgatp register into a supervisor physical address, the two stage
translation are known as VS-stage and G-stage translation

For the two stage address translation you should read the RISC-V Instruction Set Manual, Volume II: Privileged Architecture, Version 1.11
chapter hypervisor to understand different terminoly associated with it and get a hold what all goes in two stage translation.
Below is a brief description of two stage address translation followed by a diagram for better understanding

Current Virtulization mode V indicates wheather the virtual memory address requires two stage address tranlation(V=1) or a single stage 
address translation(V=0).   

.. figure:: latex wala.png
    :align: center
Note for a n-level ptw, n is bounded by two things first one is the virtualization mode the ptw is
happening which bounds the max value of n and the page that virtual address belongs to which tell
at which level the ptw will get the ppn from for example for a sv39 mode where the virtual address
belongs to 2mib page then the ptw will get the ppn from 2-level ptw
Description below assumes that the GVA and GPA belong to 4KiB page, and the VS mode is sv39
and HS mode is sv39x4 The virtual address generated by the core in VS mode need to get translated
to physical memory
1. Initially, the core is in vs mode so the virtual address GVA will first get translated to GPA. the
value of GPA is the concatenation of vssatp.ppn and gva[2].
2. Gpa is then translated to hpa usig hgatp csr, by a 3-level page table walk. This PTW is the
G-stage translation of gpa to hpa
3. The final hpa that is the translation of gpa is created by concatenating the pte.ppn of last level
ptw ,in gstage translation and offset which we get from the 12 LSB of GPA
4. This HPA is a physical address of GPA in the host machine so a memory access is done using
this address to figure out the PTE the GPA points to.
5. The response of memory access is a pte and the ppn is concatenated with gva[1] to get the gpa2.
6. The above step from step 2 to 5 are 1st level ptw of Vs-stage translation, so the steps 2-5 are
repeated 2 more times with appropriate gpa and gva values to for a 3 level PTW in vs mode
of translating the gva to gpa, after we get the final gpa a final G-stage translation is done to
converte the gpa to hpa and appropriate values are send to response of the ptw which mainly
consist of requested GVA whose translatio nis requested, then the GPA along with its permissions
bits which are used for the final g-stage translation and hpa along with its permission bits that
we get at end of whole ptw

    
.. figure:: mera-rough notebook wala walk.png
    :align: center


currently we have two package for Page table walk one in ptwalk_rv_hypervisor and the other one in ptwalk_rv_supervisor
and the selection for ptwalk is based on the fact that is the hypervisor enabled or not in the core sepcified in the core.yaml 
file. For the sake of this discussion the ptwalk_rv_hypervisor will be refered as PTW_H and the ptwalk_rv_supervisor will be
refered as PTW_S
The hypervisor enabled page table walk i.e ptwalk_rv_hypervisor works a little different than ptwalk_rv_supervisor, ther are few 
extra states and some changes in the working of the old states to include all the functionality the hypervisor adds.
First the interface remains same for both of the page table walk packages. 

STATE diagram expanation 
there are 5 states in PTW_H ReSendReq, WaitForMemory, GeneratePTE, GeneratePTE_S and SwitchPTW. Below are the description of each 
of the state separately and at the end we will walk through a fully translation for better understanding

GeneratePTE, WaitForMemory and SwitchPTW works differently depending if the ptwalk is undergoing Stage1 translation or Stage2 translation 
To decide between which stage the States are working in we have used a register rg_v which which will be basically used to check wheather 
we are in stage1/VS-stage/rg_v=1 or stage2/G-stage/rg_v=0 

GeneratePTE (default state)
first it checks is the PTW_H is in stage1 or stage2 
then check for transparent translation 

