python -m configure.main -ispec sample_config/rv64imacsu/isa.yaml \
  -customspec sample_config/rv64imacsu/custom.yaml \
  -cspec sample_config/rv64imacsu/core.yaml \
  -gspec sample_config/rv64imacsu/csr_grouping.yaml \
  -dspec sample_config/rv64imacsu/debug.yaml  --verbose debug
