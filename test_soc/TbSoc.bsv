//See LICENSE.iitm for license details
/*
Author: Neel Gala
Email id: neelgala@gmail.com
Details:

--------------------------------------------------------------------------------------------------
*/
package TbSoc;
`ifdef async_reset
  import RegOverrides  :: *;
`endif
  import Soc          :: * ;
`ifdef debug
  import DebugSoc     :: * ;
`endif
  import Clocks       :: * ;
  import GetPut       :: * ;
	import Semi_FIFOF   :: * ;
	import axi4         :: * ;
  import uart         :: * ;
	import ccore_types  :: * ;
	import csrbox_decoder :: * ;
  import csrbox :: * ;
  import DReg         :: * ;
  import Connectable  :: * ;
  import DCBus        :: * ;
  import Vector       :: * ;
  `ifdef spfpu
  import fbox        :: * ;
  `endif
  `include "csrbox.defines"
  `include "ccore_params.defines"
  `include "Logger.bsv"
  `include "Soc.defines"
`ifdef debug
  import DebugSoc     :: * ;
`endif

`ifdef openocd
  import "BDPI" function ActionValue #(int) init_rbb_jtag(Bit#(1) dummy);
  import "BDPI" function ActionValue #(Bit #(8))get_frame(int client_fd);
  import "BDPI" function Action send_tdo(Bit #(1) tdo , int client_fd);
`endif
    function Bit#(`xlen) fn_atomic_op (Bit#(5) op,  Bit#(`xlen) rs2,  Bit#(`xlen) loaded);
      Bit#(`xlen) op1 = loaded;
      Bit#(`xlen) op2 = rs2;
    `ifdef RV64
      if(op[4]==0)begin
	  		op1=signExtend(loaded[31:0]);
        op2= signExtend(rs2[31:0]);
      end
    `endif
      Int#(`xlen) s_op1 = unpack(op1);
	  	Int#(`xlen) s_op2 = unpack(op2);

      case (op[3:0])
	  			'b0011:return op2;
	  			'b0000:return (op1+op2);
	  			'b0010:return (op1^op2);
	  			'b0110:return (op1&op2);
	  			'b0100:return (op1|op2);
	  			'b1100:return min(op1,op2);
	  			'b1110:return max(op1,op2);
	  			'b1000:return pack(min(s_op1,s_op2));
	  			'b1010:return pack(max(s_op1,s_op2));
	  			default:return op1;
	  		endcase
    endfunction
  (*synthesize*)
  module [Module] mkTbSoc(Empty);

    let def_clk <- exposeCurrentClock;
    let def_rst <- exposeCurrentReset;

    MakeClockIfc#(Bit#(1)) tck_clk <-mkUngatedClock(1);
    MakeResetIfc trst <- mkReset(0,False,tck_clk.new_clk);

    /*doc:reg: */
    // Reg#(Bit#(32)) rg_timeout_count <- mkReg(0);
    /*doc:reg: */
    Reg#(Bit#(32)) rg_inst_count <- mkReg(0);

    Vector#(100, String) string_field;
    for (Integer i = 0; i<100; i = i + 1) begin
      string_field[i] = integerToString(i);
    end

  `ifdef debug
    Ifc_DebugSoc soc <- mkDebugSoc(tck_clk.new_clk, trst.new_rst);
  `else
    Ifc_Soc soc <- mkSoc();
  `endif
  `ifdef rtldump
    `include "csr_probe.bsv"
    Wire#(Bit#(`xlen)) wr_mstatus <- mkBypassWire();
    `ifdef hypervisor 
      Wire#(Bit#(`xlen)) wr_hstatus <- mkBypassWire();
    `endif
    Wire#(Bit#(`xlen)) wr_fcsr <- mkBypassWire();
  `endif
    IWithDCBus#(DCBus#(`paddr,32), Ifc_uart#(16)) uart <- mkuart_block(5);
    Reg#(Bool) rg_read_rx<- mkDReg(False);

    Reg#(Bit#(5)) rg_cnt <-mkReg(0);

    Reg#(Bit#(32)) rg_prev_stime <- mkReg(0);

    rule display_eol;
	    let timeval <- $time;
      `logLevel( tb, 0, $format("\n[%10d]", timeval))
    endrule:display_eol

  `ifdef rtldump
    rule rl_set_mstatus;
      wr_mstatus <= fn_probe_csr(`MSTATUS);
    endrule
    `ifdef hypervisor 
      rule rl_set_hstatus;
        wr_hstatus <= fn_probe_csr(`HSTATUS);
      endrule
    `endif
    rule rl_set_fcsr;
      wr_fcsr <= fn_probe_csr(`FCSR);
    endrule
 	  let dump <- mkReg(InvalidFile) ;
    rule rl_open_file_rtldump(rg_cnt<1);
      let generate_dump <- $test$plusargs("rtldump");
      if(generate_dump) begin
        String dumpFile = "rtl.dump" ;
    	  File lfh <- $fopen( dumpFile, "w" ) ;
    	  if ( lfh == InvalidFile )begin
    	    `logLevel( tb, 0, $format("TB: cannot open %s", dumpFile))
    	    $finish(0);
    	  end
    	  dump <= lfh ;
      end
    endrule:rl_open_file_rtldump
  `endif

 	  let dump1 <- mkReg(InvalidFile) ;
    rule rl_open_file_app(rg_cnt<1);
      String dumpFile1 = "app_log" ;
    	File lfh1 <- $fopen( dumpFile1, "w" ) ;
    	if (lfh1==InvalidFile )begin
    	  `logLevel( tb, 0, $format("TB: cannot open %s", dumpFile1))
    	  $finish(0);
    	end
      dump1 <= lfh1;
    	rg_cnt <= rg_cnt+1 ;
    endrule:rl_open_file_app

    rule rl_connect_uart_out;
      soc.uart_io.sin(uart.device.io.sout);
    endrule:rl_connect_uart_out

    rule rl_connect_uart_in;
      uart.device.io.sin(soc.uart_io.sout);
    endrule:rl_connect_uart_in

    rule rl_check_if_character_present(!rg_read_rx);
      let {err, data}<- uart.dcbus.read('hc,Sz1,PvM);
      if (data[3]==1) // character present
        rg_read_rx<=True;
    endrule:rl_check_if_character_present

    rule rl_write_received_character(rg_cnt>=1 && rg_read_rx);
      let {err,data}<-uart.dcbus.read('h8,Sz1, PvM);
      $fwrite(dump1,"%c",data);
    endrule:rl_write_received_character


  `ifdef rtldump
    // `define limit 'd5000000
    rule rl_write_dump_file(rg_cnt>=1);
      let print_instr_latency <- $test$plusargs("instr_latency");
      let generate_dump <- $test$plusargs("rtldump");
      let halt_on_selfloop <- $test$plusargs("halt_selfloop");
      let print_stime <- $test$plusargs("stime");
      let split_dump <-$test$plusargs("split_dump");

      // if (rg_timeout_count > 10000) begin
      //   $display("No instruction committed for 10000 cycles. Ending Simulation");
      //   $finish(0);
      // end

      if (soc.commitlog matches tagged Valid .idump) begin
        Bit#(2) op_lsb = idump.instruction[1:0];
        Bit#(5) op_msb = idump.instruction[6:2];
        Bool is_float = op_lsb == 3 && (op_msb == 5'b00001 || op_msb == 5'b01001 || 
                          op_msb == 5'b10000 || op_msb == 5'b10010 || op_msb == 5'b10011 || 
                          op_msb == 5'b10001 || op_msb == 5'b10100); 
        Bit#(6) inst_msb = truncateLSB(idump.instruction);
        Bool is_mv = op_msb == 5'b10100 && (inst_msb == 6'b111000); 
        Bool is_fsgnj = op_msb == 5'b10100 && (inst_msb == 6'b001000); 
        Bool is_frf_mv = op_msb == 5'b10100 && inst_msb == 6'b111100;
        Bool is_fcmp = op_msb == 5'b10100 && inst_msb == 6'b101000;
        Bool is_fcvt = op_msb == 5'b10100 && inst_msb == 6'b110000;
      // rg_timeout_count <= 0;
      if ( halt_on_selfloop && (idump.instruction=='h00006f||idump.instruction =='h00a001))
        $finish(0);
      else if(generate_dump)begin
        if (rg_inst_count % `limit == 0 && rg_inst_count != 0 && split_dump) begin
          File lfh1 <- $fopen( "rtl"+string_field[rg_inst_count/`limit ]+".dump","w");
          dump <= lfh1;
        end
        rg_inst_count <= rg_inst_count + 1;
        let stime <- $stime;
        if (print_instr_latency)
          $fwrite(dump,"[%10d] ",stime-rg_prev_stime);
        rg_prev_stime <= stime;
        if (print_stime)
          $fwrite(dump, stime," ");
        if (idump.instruction[1:0] == 'b11)
        	$fwrite(dump, "core   0: ", idump.mode, " 0x%16h", idump.pc, " (0x%8h", idump.instruction, ")");
        else
          $fwrite(dump, "core   0: ", idump.mode, " 0x%16h", idump.pc, " (0x%4h", idump.instruction[15:0], ")");

        if (idump.inst_type matches tagged REG .d) begin
          Bit#(`xlen) xdata = truncate(d.wdata);
          `ifdef spfpu
          Recfmt#(`expwidth,`sigwidth) x = truncate(d.wdata);
          Bit#(`flen) fdata = to_ieee(x);
          if(is_float && !is_mv) begin
            let fcsr = wr_fcsr;
            Bit#(5) flags = truncate(fcsr);
            Bit#(`xlen) wdata = zeroExtend(flags);
            let frm = fcsr >> 5;
            if(!is_frf_mv && !is_fsgnj) begin
            if (valueOf(`xlen) == 64) 
              $fwrite(dump, " " , fn_csr_to_str(`FFLAGS), " 0x%16h", wdata);
            if (valueOf(`xlen) == 32)
              $fwrite(dump, " " , fn_csr_to_str(`FFLAGS), " 0x%8h", wdata);
            end
            if (valueOf(`xlen) == 64) 
              $fwrite(dump, " " , fn_csr_to_str(`MSTATUS), " 0x%16h", wr_mstatus);
            if (valueOf(`xlen) == 32)
              $fwrite(dump, " " , fn_csr_to_str(`MSTATUS), " 0x%8h", wr_mstatus);
          end
          `endif
          if (!(idump.instruction[31:25] =='b0001001 && idump.instruction[14:0] == 'b000000001110011)) begin
            if (d.irf && valueOf(`xlen) == 64 && d.rd != 0)
              $fwrite(dump, " x%d", d.rd, " 0x%16h", xdata);
            if (d.irf && valueOf(`xlen) == 32 && d.rd != 0)
              $fwrite(dump, " x%d", d.rd, " 0x%8h", xdata);
            `ifdef spfpu
            if (!d.irf && valueOf(`flen) == 64)
              $fwrite(dump, " f%d", d.rd, " 0x%16h", fdata);
            if (!d.irf && valueOf(`flen) == 32)
              $fwrite(dump, " f%d", d.rd, " 0x%8h", fdata);
            `endif
          end
        end

        if (idump.inst_type matches tagged CSR .d) begin
          let csr_address = d.csr_address;
          let csr_updated = d.csr_updated;

          if (valueOf(`xlen) == 64 && d.rd != 0)
            $fwrite(dump, " x%d", d.rd, " 0x%16h", d.rdata);
          if (valueOf(`xlen) == 32 && d.rd != 0)
            $fwrite(dump, " x%d", d.rd, " 0x%8h", d.rdata);
          Bit#(`xlen) wdata;
          if(csr_address == `FCSR) begin
            let fcsr = wr_fcsr;
            Bit#(5) flags = truncate(fcsr);
            wdata = zeroExtend(flags);
            let frm = fcsr >> 5;
            if (!((d.op==2'b10 || d.op == 2'b11) && idump.instruction[19:15] == 0) && d.csr_updated) begin
              /* if(pack(idump.mode)==3) begin */
              if (valueOf(`xlen) == 64) 
                $fwrite(dump, " " , fn_csr_to_str(`FRM), " 0x%16h", frm);
              if (valueOf(`xlen) == 32)
                $fwrite(dump, " " , fn_csr_to_str(`FRM), " 0x%8h", frm);
              if (valueOf(`xlen) == 64) 
                $fwrite(dump, " " , fn_csr_to_str(`FFLAGS), " 0x%16h", wdata);
              if (valueOf(`xlen) == 32)
                $fwrite(dump, " " , fn_csr_to_str(`FFLAGS), " 0x%8h", wdata);
              let mstatus = wr_mstatus;
                if (valueOf(`xlen) == 64) 
                  $fwrite(dump, " " , fn_csr_to_str(`MSTATUS), " 0x%16h", mstatus);
                if (valueOf(`xlen) == 32)
                  $fwrite(dump, " " , fn_csr_to_str(`MSTATUS), " 0x%8h", mstatus);
            end
          end
          else begin
          //commenting as spike is printing the hsatus value only when it is being updated but this 
          //printing hstatus value always
          `ifdef hypervisor
            if ((idump.instruction[31:0] =='h10200073) ) begin // 
            // if (csr_address == `SSTATUS) begin
              if (valueOf(`xlen) == 64) 
                $fwrite(dump, " " , fn_csr_to_str(`HSTATUS), " 0x%16h", wr_hstatus);
              if (valueOf(`xlen) == 32)
                $fwrite(dump, " " , fn_csr_to_str(`HSTATUS), " 0x%8h", wr_hstatus);
            end
          `endif
          `ifdef supervisor
            if (csr_address == `SSTATUS || csr_address == `SIE || csr_address == `SIP) begin
              csr_address = csr_address + 'h200;
            end
          `endif
            wdata = fn_probe_csr(csr_address);
            if (!((d.op==2'b10 || d.op == 2'b11) && idump.instruction[19:15] == 0) && csr_updated) begin
              if (valueOf(`xlen) == 64) 
                $fwrite(dump, " " , fn_csr_to_str(csr_address), " 0x%16h", wdata);
              if (valueOf(`xlen) == 32)
                $fwrite(dump, " " , fn_csr_to_str(csr_address), " 0x%8h", wdata);
            end
            let mstatus = wr_mstatus;
            if((csr_address == `FFLAGS || csr_address == `FRM) && csr_updated) begin
              if (valueOf(`xlen) == 64) 
                $fwrite(dump, " " , fn_csr_to_str(`MSTATUS), " 0x%16h", mstatus);
              if (valueOf(`xlen) == 32)
                $fwrite(dump, " " , fn_csr_to_str(`MSTATUS), " 0x%8h", mstatus);
            end
          end
        end

        if (idump.inst_type matches tagged MEM .d) begin
          Bit#(`xlen) store_data = truncate(d.data);
        `ifdef atomic
          if (d.access == Atomic && d.atomic_op[3:0] != 5 && d.atomic_op[3:0] != 7) begin
            store_data = fn_atomic_op(d.atomic_op,truncate(d.data), truncate(d.commit_data));
          end
        `endif
          let mstatus = wr_mstatus;
          if(!d.irf && d.access == Load) begin
            if (valueOf(`xlen) == 64) 
              $fwrite(dump, " " , fn_csr_to_str(`MSTATUS), " 0x%16h", mstatus);
            if (valueOf(`xlen) == 32)
              $fwrite(dump, " " , fn_csr_to_str(`MSTATUS), " 0x%8h", mstatus);
          end
          if (d.access == Load `ifdef atomic || d.access == Atomic `endif ) begin
            Bit#(`xlen) wdata = truncate(d.commit_data);
            if (d.irf && valueOf(`xlen) == 64 && d.rd != 0)
              $fwrite(dump, " x%d", d.rd, " 0x%16h", wdata);
            if (d.irf && valueOf(`xlen) == 32 && d.rd != 0)
              $fwrite(dump, " x%d", d.rd, " 0x%8h", wdata);
            if (!d.irf && valueOf(`flen) == 64 )
              $fwrite(dump, " f%d", d.rd, " 0x%16h", wdata);
            if (!d.irf && valueOf(`flen) == 32 )
              $fwrite(dump, " f%d", d.rd, " 0x%8h", wdata[31:0]);
          end

          if(valueOf(`xlen) ==64 && d.access != Fence && d.access != FenceI)
            $fwrite(dump, " mem 0x%16h", d.address);
          if(valueOf(`xlen) ==32&& d.access != Fence && d.access != FenceI)
            $fwrite(dump, " mem 0x%8h", d.address);

        `ifdef atomic
          if (d.access == Atomic && d.atomic_op[3:0] != 5 && d.atomic_op[3:0] != 7) begin
            if(valueOf(`xlen) ==64)
              $fwrite(dump, " mem 0x%16h", d.address);
            if(valueOf(`xlen) ==32)
              $fwrite(dump, " mem 0x%8h", d.address);
          end
        `endif

          if (d.access == Store  `ifdef atomic || (d.access == Atomic && d.atomic_op[3:0] != 5) `endif ) begin
            if (d.size == 0) begin
              if (store_data[7:4]==0)
                $fwrite(dump, " 0x%1h", store_data[3:0]);
              else
                $fwrite(dump, " 0x%2h", store_data[7:0]);
            end
            if (d.size == 1)
              $fwrite(dump, " 0x%4h", store_data[15:0]);
            if (d.size == 2)
              $fwrite(dump, " 0x%8h", store_data[31:0]);
            if (d.size == 3)
              $fwrite(dump, " 0x%16h", store_data);
          end
        end
          $fwrite(dump, "\n");
      end

      end
      // else
      //   rg_timeout_count <= rg_timeout_count + 1;
    endrule
  `endif

  `ifdef debug
    Wire#(Bit#(1)) wr_tdi <-mkWire();
    Wire#(Bit#(1)) wr_tms <-mkWire();
    rule connect_jtag_io;
      soc.wire_tdi(wr_tdi);
      soc.wire_tms(wr_tms);
    endrule
  `endif
  `ifdef openocd
    Wire#(Bit#(1)) wr_tdo <-mkWire();
    Wire#(Bit#(1)) wr_tck <-mkWire();
    Wire#(Bit#(1)) wr_trst <-mkWire();
    rule rl_wr_tdo;
      wr_tdo <= soc.wire_tdo();
    endrule
    Reg#(Bit#(1)) rg_initial <- mkRegA(0);
    Reg#(Bit#(1)) rg_end_sim <- mkRegA(0);
    Reg#(int) rg_client_fd <- mkRegA(32'hffffffff);
    Reg#(Bit#(5)) delayed_actor <- mkReg(0);
    Reg#(Bit#(5)) delayed_actor2 <- mkReg(0);
    Reg#(Bit#(5)) delayed_actor3 <- mkReg(0);
    Reg#(Bit#(5)) delayed_actor4 <- mkReg(0);
    Reg#(Bit#(5)) delayed_actor5 <- mkReg(0);
    rule rl_initial(rg_initial == 0);
      let x <- init_rbb_jtag(0);
      if(x != 32'hffffffff)begin
        rg_initial <= 1'b1;
        rg_client_fd <= x;
      end
    endrule
    rule rl_get_frame((rg_initial == 1'b1));
      let x <- get_frame(rg_client_fd);
      delayed_actor <= truncate(x);
      delayed_actor2 <= delayed_actor;
      delayed_actor3 <= delayed_actor2;
      delayed_actor4 <= delayed_actor3;
      delayed_actor5 <= delayed_actor4;
      tck_clk.setClockValue(delayed_actor2[2]);
      if(delayed_actor2[4] == 1)
        trst.assertReset();
      if(delayed_actor5[3] == 1 )
        send_tdo(wr_tdo,rg_client_fd);
      wr_tdi <= delayed_actor[0];
      wr_tms <= delayed_actor[1];
      if( x[5] == 1)begin
        $display("OpenOcd Exit");
        $finish();
      end
    endrule
  `endif
  endmodule
endpackage: TbSoc
