# see LICENSE.incore for licensing details

from cerberus import Validator
from configure.utils import yaml
import configure.utils as utils
from configure.errors import ValidationError
from configure.consts import *
import os
import logging
import sys
import math
from repomanager.rpm import repoman
from riscv_config.warl import warl_interpreter
from csrbox.csr_gen import find_group

logger = logging.getLogger(__name__)

bsc_path = utils.which("bsc")[:-7]

bsv_path_file = open('bsvpath','r').read().splitlines()

def check_prerequisites():
    utils.which('bsc')
    utils.which('bluetcl')
    utils.which('csrbox')
    utils.which('riscv-config')

def clone_dependencies(verbose,clean,update,patch):
    repoman(dependency_yaml,clean,update,patch,False,'./')

def genus_synth_script(uarch_yaml, isa_yaml):
    tech_size = uarch_yaml['asic_params']['tech_size']
    freq_mhz = uarch_yaml['asic_params']['frequency_mhz']
    top_module = uarch_yaml['bsc_compile_options']['top_module']
    retime_list = ''
    if 'M' in isa_yaml['hart0']['ISA'] or 'Zmmul' in isa_yaml['hart0']['ISA']:
        retime_list += ' mkcombo_mul'
    if 'F' in isa_yaml['hart0']['ISA'] :
        retime_list += ' mkspfma'
    if 'D' in isa_yaml['hart0']['ISA'] :
        retime_list += ' mkdpfma'
    verilog_dir = os.path.abspath(uarch_yaml['bsc_compile_options']['verilog_dir'])
    work_dir = uarch_yaml['bsc_compile_options']['work_dir']
    dump_file = open(f'{work_dir}/chromite_genus_synth.tcl','w')
    with open('configure/templates/chromite_genus_synth.tcl','r') as f:
        content = f.readlines()
    for line in content:
        dump_file.write(eval("f'" + line.rstrip() + "'"))
        dump_file.write("\n")
    dump_file.close()
    logger.info(f'Generating Genus Synthesis script at: {work_dir}/chromite_genus_synth.tcl')



def uarch_specific_checks(foo, isa_string):

    logger.info('Performing CORE YAML Specific Checks')

    xlen = 64
    max_value = 2 ** 64
    if '32' in isa_string:
        max_value = 2 ** 32
        xlen = 32

    if foo['merged_rf']:
        if 'F' not in isa_string:
            logger.error('merged_rf should be True only when F support is \
                    available in hw')
            raise SystemExit(1)

    # check if default values are correctly assigned
    for field in length_check_fields:
        if foo[field] > (max_value-1):
            logger.error('Default Value of ' + field + ' exceeds the max\
 allowed value')
            raise SystemExit(1)

    # check a_extension
    if 'A' in isa_string:
        res_sz = foo['a_extension']['reservation_size']
        if not (res_sz and (not(res_sz & (res_sz - 1)))):
            logger.error('reservation_size must be power of 2')
            raise SystemExit(1)
        if xlen == 64 and res_sz < 8:
            logger.error('For RV64 reservation size must be minimum 8 bytes')
            raise SystemExit(1)

    # check m_extension
    m_mulstages_in = foo['m_extension']['mul_stages_in']
    m_mulstages_out = foo['m_extension']['mul_stages_out']
    m_divstages = foo['m_extension']['div_stages']
    if 'M' in isa_string:
        if m_mulstages_in +m_mulstages_out> xlen:
            logger.error('Multiplication stages cannot exceed XLEN')
            raise SystemExit(1)
        if m_divstages > xlen:
            logger.error('Division stages cannot exceed XLEN')
            raise SystemExit(1)

    # check icache
    icache_enable = foo['icache_configuration']['instantiate']
    i_words = foo['icache_configuration']['word_size']
    i_blocks = foo['icache_configuration']['block_size']
    i_sets = foo['icache_configuration']['sets']
    i_ways = foo['icache_configuration']['ways']
    if icache_enable and 'S' in isa_string:
        if i_words*i_sets*i_blocks > 4096:
            logger.error('Since Supervisor is enabled, each way of I-Cache\
 should be less than 4096 Bytes')
            raise SystemExit(1)

    # check dcache
    dcache_enable = foo['dcache_configuration']['instantiate']
    d_words = foo['dcache_configuration']['word_size']
    d_blocks = foo['dcache_configuration']['block_size']
    d_sets = foo['dcache_configuration']['sets']
    d_ways = foo['dcache_configuration']['ways']
    if dcache_enable:
      if xlen != (d_words * 8):
        logger.error('D_WORDS for a '+str(xlen)+'-bit core should be '+
          str(xlen/8))
        raise SystemExit(1)
    if dcache_enable and 'S' in isa_string:
        if i_words*i_sets*i_blocks > 4096:
            logger.error('Since Supervisor is enabled, each way of D-Cache\
 should be less than 4096 Bytes')
            raise SystemExit(1)
        if d_words * 8 != xlen:
            logger.error('D-Cache d_words should be ' + str(xlen/8))
            raise SystemExit(1)

    if foo['bsc_compile_options']['ovl_assertions']:
        if foo['bsc_compile_options']['ovl_path'] is None or \
                foo['bsc_compile_options']['ovl_path'] == '':
                    logger.error('Please set ovl_path in core spec')
                    raise SystemExit(1)

def gen_bsc_defines(foo, isa_node, debug_spec, grouping_spec):
    global bsc_defines

    logger.info('Generating BSC defines')
    xlen = 64
    if '32' in isa_node['ISA']:
         xlen = 32
    if 'S' in isa_node['ISA']:
        page_offset = 12
        if xlen == 64:
            ppnsize = 44
            satp_mode_size = 4
        else:
            ppnsize = 22
            satp_mode_size = 1
        satp_modewarl =\
                (warl_interpreter(isa_node['satp']['rv'+str(xlen)]['mode']['type']['warl']))
        if xlen == 64 and satp_modewarl.islegal(0xa,[]):
            s_mode = 'sv57'
            max_varpages = 5
            subvpn = 9
            lastppnsize = 8
            maxvaddr = 57
            vpnsize = 45
        elif xlen == 64 and satp_modewarl.islegal(9,[]):
            s_mode = 'sv48'
            max_varpages = 4
            subvpn = 9
            lastppnsize = 17
            maxvaddr = 48
            vpnsize = 36
        elif xlen == 64 and satp_modewarl.islegal(8,[]):
            s_mode = 'sv39'
            max_varpages = 3
            subvpn = 9
            lastppnsize = 26
            maxvaddr = 39
            vpnsize = 27
        elif xlen == 32 and satp_modewarl.islegal(1,[]):
            s_mode = 'sv32'
            max_varpages = 2
            subvpn = 10
            lastppnsize = 12
            maxvaddr = 32
            vpnsize = 32
        else:
            logger.error('Cannot deduce supervisor mode from satp.mode')
            raise SystemExit(1)

        asidlen = 0
        asid_mask = 0xFFFF
        satp_asidwarl =\
                (warl_interpreter(isa_node['satp']['rv'+str(xlen)]['asid']['type']['warl']))
        while asid_mask != 0:
            if satp_asidwarl.islegal(int(asid_mask), []):
                asidlen = int(math.log2(asid_mask+1))
                break
            else:
                asid_mask = asid_mask >> 1

    if 'H' in isa_node['ISA']:
        vmidlen = 0
        vmid_mask = 0x00FF
        hgatp_asidwarl =\
                (warl_interpreter(isa_node['hgatp']['rv'+str(xlen)]['vmid']['type']['warl']))
        while vmid_mask != 0:
            if hgatp_asidwarl.islegal(int(vmid_mask), []):
                vmidlen = int(math.log2(vmid_mask+1))
                break
            else:
                vmid_mask = vmid_mask >> 1        

    flen = xlen
    elen = xlen
    if 'D' in isa_node['ISA']:
        instantiate_f = True
        flen = 64
        elen = max(xlen,65)
    elif 'F' in isa_node['ISA']:
        flen = 32
        elen = max(xlen,33)


    m_mulstages_in = foo['m_extension']['mul_stages_in']
    m_mulstages_out = foo['m_extension']['mul_stages_out']
    m_divstages = foo['m_extension']['div_stages']

    fcfg = foo['fd_extension']

    index = {'pre':1,'mac':2,'post':4,'round':8}

    spfma_iregs = []
    spfma_oregs = []
    dpfma_iregs = []
    dpfma_oregs = []
    spfma_mods = []
    dpfma_mods = []
    optimal_forder = 0

    if 'spfma' in fcfg:
        mods = 0
        for element in fcfg['spfma']:
            mod = (fcfg['spfma'][element]['mod']).lower()
            temp = 0
            for x in index:
                if x in mod:
                    temp = temp | index[x]
            spfma_mods.append(fcfg['spfma'][element]['mod'])
            spfma_iregs.append( fcfg['spfma'][element]['in'] )
            spfma_oregs.append( fcfg['spfma'][element]['out'] )
            if temp & mods != 0:
                logger.error("Overlap in module stages of SPFMA.")
                raise SystemExit
            mods = mods | temp
        if mods != 15:
            logger.error("Incomplete SPFMA module description.")
    if 'dpfma' in fcfg:
        mods = 0
        for element in fcfg['dpfma']:
            mod = (fcfg['dpfma'][element]['mod']).lower()
            temp = 0
            for x in index:
                if x in mod:
                    temp = temp | index[x]
            dpfma_mods.append(fcfg['dpfma'][element]['mod'])
            dpfma_iregs.append( fcfg['dpfma'][element]['in'] )
            dpfma_oregs.append( fcfg['dpfma'][element]['out'] )
            if temp & mods != 0:
                logger.error("Overlap in module stages of DPFMA.")
                raise SystemExit
            mods = mods | temp
        if mods != 15:
            logger.error("Incomplete DPFMA module description.")

    optimal_forder = sum(spfma_iregs) + sum(spfma_oregs) + sum(dpfma_iregs) + sum(dpfma_oregs) + 2

    mhpm_eventcount = foo['total_events']

    limit = foo['bsc_compile_options']['trace_dump_limit']
    test_memory_size = foo['bsc_compile_options']['test_memory_size']
    test_memory_size = math.log2(test_memory_size)
    macros = 'Addr_space='+str(int(test_memory_size))
    macros += ' xlen='+str(xlen)
    if flen == 32:
        macros += ' sigwidth=24 expwidth=8'
    elif flen == 64:
        macros += ' sigwidth=53 expwidth=11'
    macros += ' flen='+str(flen)
    macros += ' elen='+str(elen)
    macros += ' bypass_sources=2'
    if foo['bsc_compile_options']['cocotb_sim']:
        macros += ' cocotb_sim'
    if debug_spec is not None:
        macros += ' debug'
        macros += ' debug_bus_sz='+str(xlen)

    if foo['bsc_compile_options']['assertions']:
        macros += ' ASSERT'
    if foo['bsc_compile_options']['trace_dump']:
        macros += ' rtldump'
    if foo['bsc_compile_options']['ovl_assertions']:
        macros += ' ovl_assert'
    if foo['bsc_compile_options']['sva_assertions']:
        macros += ' sva_assert'
    if foo['bsc_compile_options']['trace_dump_limit']:
        macros += ' limit='+str(limit)

    for isb, isb_val in foo['isb_sizes'].items():
        macros += ' {0}={1}'.format(isb, isb_val)

    if foo['overlap_redirections']:
        macros += ' overlap_redirections'

    macros += ' RV'+str(xlen)+' ibuswidth='+str(xlen)
    macros += ' dbuswidth='+str(max(xlen,flen))
    macros += ' resetpc='+str(foo['reset_pc'])
    macros += ' paddr='+str(isa_node['physical_addr_sz'])
    macros += ' vaddr='+str(xlen)
    macros += ' CORE_'+str(foo['bus_protocol'])
    macros += ' iesize='+str(foo['iepoch_size'])
    macros += ' desize='+str(foo['depoch_size'])
    macros += ' num_harts='+str(foo['num_harts'])
    macros += ' microtrap_support'

    wawid = foo['isb_sizes']['isb_s3s4']+foo['isb_sizes']['isb_s4s5']

    # TODO: Fix calculation of wawid size
    # wawid = int(math.ceil(math.log2(wawid)))
    wawid = int(math.ceil(math.log2(wawid+optimal_forder)))

    if not foo['waw_stalls']:
        macros += ' no_wawstalls'
        macros += ' wawid='+str(wawid)
    else:
        macros += ' wawid=0'

    if foo['bsc_compile_options']['compile_target'] == 'sim':
        macros += ' simulate'
    if foo['bsc_compile_options']['open_ocd']:
        macros += ' openocd'
    if foo['bsc_compile_options']['elfmem']:
        macros += ' elfmem'
    if foo['bsc_compile_options']['async_reset']:
        macros += ' async_reset'

    macros += ' mhpm_eventcount=' + str(mhpm_eventcount)

    if 'A' in isa_node['ISA']:
        macros += ' atomic'
        macros += ' reservation_sz='+str(foo['a_extension']['reservation_size'])
    if 'F' in isa_node['ISA'] or 'D' in isa_node['ISA']:
        if fcfg['ordering_depth'] < optimal_forder:
            logger.warning('FD ordering should be greater than the total pipeline depth of F submodules for optimal performance')
        macros += ' spfpu'
        for i,entry in enumerate(spfma_mods):
            macros += ' SPFMA_STAGE_{0}={1}'.format(i,entry)
        for i,entry in enumerate(spfma_iregs):
            macros += ' SPFMA_STAGE_{0}_IN={1}'.format(i,entry)
        for i,entry in enumerate(spfma_oregs):
            macros += ' SPFMA_STAGE_{0}_OUT={1}'.format(i,entry)
        for i,entry in enumerate(dpfma_oregs):
            macros += ' DPFMA_STAGE_{0}_OUT={1}'.format(i,entry)
        for i,entry in enumerate(dpfma_iregs):
            macros += ' DPFMA_STAGE_{0}_IN={1}'.format(i,entry)
        for i,entry in enumerate(dpfma_mods):
            macros += ' DPFMA_STAGE_{0}={1}'.format(i,entry)
        if 'D' in isa_node['ISA']:
            macros += ' dpfpu'
        macros += ' FORDERING_DEPTH='+str( fcfg['ordering_depth'] )
    if 'C' in isa_node['ISA']:
        macros += ' compressed'
    if 'M' in isa_node['ISA']:
        macros += ' muldiv'
        macros += ' MULSTAGES_IN='+str(m_mulstages_in)
        macros += ' MULSTAGES_OUT='+str(m_mulstages_out)
        macros += ' MULSTAGES_TOTAL='+str(m_mulstages_out+m_mulstages_in)
        macros += ' DIVSTAGES='+str(m_divstages)
    if 'Zicsr' in isa_node['ISA']:
        macros += ' zicsr'
    if 'U' in isa_node['ISA']:
        macros += ' user'
    if 'N' in isa_node['ISA']:
        macros += ' usertraps'
    if 'H' in isa_node['ISA']:
        macros += ' hypervisor'
        macros += ' vmidwidth='+str(vmidlen)    
    if 'S' in isa_node['ISA']:
        macros += ' supervisor'
        macros += ' asidwidth='+str(asidlen)
        macros += ' ' + s_mode
        macros += ' ppnsize='+str(ppnsize)
        macros += ' page_offset='+str(page_offset)
        macros += ' satp_mode_size='+str(satp_mode_size)
        macros += ' max_varpages='+str(max_varpages)
        macros += ' subvpn='+str(subvpn)
        macros += ' lastppnsize='+str(lastppnsize)
        macros += ' maxvaddr='+str(maxvaddr)
        macros += ' vpnsize='+str(vpnsize)
        if 'Svnapot' in isa_node['ISA']:
            macros += ' svnapot'

        if foo['s_extension']['sfence_i_complexity'] == 'simple' and foo['s_extension']['sfence_d_complexity'] == 'simple':
            macros += ' simpl_sfence'

        if foo['s_extension']['sfence_i_complexity'] == 'complex':
            macros += ' sfence_i_complex'
        else:
            macros += ' sfence_i_simple'
        if foo['s_extension']['sfence_d_complexity'] == 'complex':
            macros += ' sfence_d_complex'
        else:
            macros += ' sfence_d_simple'

        if 'dummy_tlb' in foo['s_extension']['dtlb_config']:
            if foo['s_extension']['dtlb_config']['dummy_tlb'] == True:
                macros += ' dtlb_dummy'
        else:
            if 'set_associative' in foo['s_extension']['dtlb_config']:
                macros += ' dtlb_sa'
                for i, items in foo['s_extension']['dtlb_config']['set_associative'].items():
                    macros += f' dtlb_sets_{i}={items["sets"]}'
                    macros += f' dtlb_ways_{i}={items["ways"]}'
                    macros += f' dtlb_rep_alg_{i}={items["replacement"]}'
            else:
                macros += ' dtlb_fa'
                macros += f" dtlbsize={foo['s_extension']['dtlb_config']['fully_associative']['tlb_size']}"
                macros += f" dtlb_rep_alg={foo['s_extension']['dtlb_config']['fully_associative']['replacement']}"
        
        if 'dummy_tlb' in foo['s_extension']['itlb_config']:
            if foo['s_extension']['itlb_config']['dummy_tlb'] == True:
                macros += ' itlb_dummy'
        else:    
            if 'set_associative' in foo['s_extension']['itlb_config']:
                macros += ' itlb_sa'
                for i, items in foo['s_extension']['itlb_config']['set_associative'].items():
                    macros += f' itlb_sets_{i}={items["sets"]}'
                    macros += f' itlb_ways_{i}={items["ways"]}'
                    macros += f' itlb_rep_alg_{i}={items["replacement"]}'
            else:
                macros += ' itlb_fa'
                macros += f" itlbsize={foo['s_extension']['itlb_config']['fully_associative']['tlb_size']}"
                macros += f" itlb_rep_alg={foo['s_extension']['itlb_config']['fully_associative']['replacement']}"
    if 'N' in isa_node['ISA'] or 'S' in isa_node['ISA']:
        macros += ' non_m_traps'
    if foo['branch_predictor']['instantiate']:
        macros += ' bpu'
        macros += ' '+foo['branch_predictor']['predictor']
        macros += ' btbdepth='+str(foo['branch_predictor']['btb_depth'])
        macros += ' bhtdepth='+str(foo['branch_predictor']['bht_depth'])
        macros += ' histlen='+str(foo['branch_predictor']['history_len'])
        macros += ' histbits='+str(foo['branch_predictor']['history_bits'])
        macros += ' rasdepth='+str(foo['branch_predictor']['ras_depth'])
        if foo['branch_predictor']['ras_depth'] > 0:
            macros += ' bpu_ras'
    if foo['merged_rf']:
        macros += ' merged_rf'

    macros += ' iwords='+str(foo['icache_configuration']['word_size'])
    macros += ' iblocks='+str(foo['icache_configuration']['block_size'])
    macros += ' iways='+str(foo['icache_configuration']['ways'])
    macros += ' isets='+str(foo['icache_configuration']['sets'])
    macros += ' ifbsize='+str(foo['icache_configuration']['fb_size'])
    if foo['icache_configuration']['one_hot_select']:
        macros += ' icache_onehot=1'
    else:
        macros += ' icache_onehot=0'
    if foo['icache_configuration']['ecc_enable']:
        macros += ' icache_ecc'
    if foo['icache_configuration']['instantiate']:
        macros += ' icache'
    if foo['icache_configuration']['instantiate'] or \
            foo['branch_predictor']['instantiate']:
        macros += ' ifence'
    if foo['icache_configuration']['replacement'] == "RANDOM":
        macros += ' irepl=0'
    if foo['icache_configuration']['replacement'] == "RR":
        macros += ' irepl=1'
    if foo['icache_configuration']['replacement'] == "PLRU":
        macros += ' irepl=2'

    macros += ' dwords='+str(foo['dcache_configuration']['word_size'])
    macros += ' dblocks='+str(foo['dcache_configuration']['block_size'])
    macros += ' dways='+str(foo['dcache_configuration']['ways'])
    macros += ' dsets='+str(foo['dcache_configuration']['sets'])
    macros += ' dfbsize='+str(foo['dcache_configuration']['fb_size'])
    macros += ' dsbsize='+str(foo['dcache_configuration']['sb_size'])
    macros += ' dlbsize='+str(foo['dcache_configuration']['lb_size'])
    macros += ' dibsize='+str(foo['dcache_configuration']['ib_size'])
    macros += ' dcache_'+str(foo['dcache_configuration']['rwports'])
    if foo['dcache_configuration']['one_hot_select']:
        macros += ' dcache_onehot=1'
    else:
        macros += ' dcache_onehot=0'
    if(foo['dcache_configuration']['ecc_enable']):
        macros += ' dcache_ecc'
    if foo['dcache_configuration']['instantiate']:
        macros += ' dcache'
    if foo['dcache_configuration']['replacement'] == "RANDOM":
        macros += ' drepl=0'
    if foo['dcache_configuration']['replacement'] == "RR":
        macros += ' drepl=1'
    if foo['dcache_configuration']['replacement'] == "PLRU":
        macros += ' drepl=2'

    macros += ' csr_low_latency'

    total_counters = 0
    pmp_entries = 0
    for node in isa_node:
        if 'mhpmcounter' in node:
            if isa_node[node]['rv'+str(xlen)]['accessible'] and \
                    find_group(grouping_spec, node) is not None:
                total_counters += 1
        if 'pmpaddr' in node:
            if isa_node[node]['rv'+str(xlen)]['accessible'] and \
                    find_group(grouping_spec, node) is not None:
                pmp_entries += 1

    if total_counters > 0:
        macros += ' perfmonitors'
    if pmp_entries > 0:
        macros += ' pmp'
        macros += ' pmpentries='+str(pmp_entries)
        macros += ' pmp_grain='+str(isa_node['pmp_granularity'])


    # reset cycle latency
    dsets = foo['dcache_configuration']['sets']
    isets = foo['dcache_configuration']['sets']
    rfset = 64 if foo['merged_rf'] else 32
    bhtsize = foo['branch_predictor']['bht_depth']
    macros += ' reset_cycles='+str(max(dsets, isets, rfset, bhtsize))

    # find the size of interrupts
    max_int_cause = 11
    max_ex_cause = 15
    for ci in isa_node['custom_interrupts']:
        max_int_cause = max(max_int_cause,ci['cause_val'])
    for ci in isa_node['custom_exceptions']:
        max_ex_cause = max(max_ex_cause,ci['cause_val'])
    max_ex_cause = max_ex_cause + 3
    macros += ' max_int_cause='+str(max_int_cause)
    macros += ' max_ex_cause='+str(max_ex_cause)
    macros += ' causesize='+str(math.ceil(math.log2(max(max_int_cause, max_ex_cause)+1))+1)

    # noinlining modules
    for module in foo['noinline_modules']:
        if foo['noinline_modules'][module]:
            macros += ' '+str(module)+'_noinline'
    return macros

def gen_make_include(foo, xlen, bsc_defines, logging=False):
    global verilator_cmd

    cwd = os.getcwd()
    make_file = open('makefile.inc','w')
    top_file = foo['bsc_compile_options']['top_file']
    top_module = foo['bsc_compile_options']['top_module']
    top_dir = foo['bsc_compile_options']['top_dir']
    verilog_dir = foo['bsc_compile_options']['verilog_dir']
    bsv_build_dir = foo['bsc_compile_options']['build_dir']
    verilator_threads = foo['verilator_configuration']['threads']

    if foo['verilator_configuration']['trace']:
        verilator_trace = '--trace'
    else:
        verilator_trace = ''
    if "none" in foo['verilator_configuration']['coverage']:
        verilator_coverage = ''
    elif "all" in foo['verilator_configuration']['coverage']:
        verilator_coverage = '--coverage'
    else:
        verilator_coverage = '--coverage-'+\
                foo['verilator_configuration']['coverage']
    verilator_speed = ''
    verilator_speed += ' OPT_FAST="{0}"'.format(foo['verilator_configuration']['opt_fast'])
    verilator_speed += ' OPT_SLOW="{0}"'.format(foo['verilator_configuration']['opt_slow'])
    verilator_speed += ' OPT="{0}"'.format(foo['verilator_configuration']['opt'])
    verilator_cmd = verilator_cmd.format(verilator_trace, verilator_coverage,
            verilator_threads)
    if foo['bsc_compile_options']['ovl_assertions']:
        verilator_cmd += ' -y '+foo['bsc_compile_options']['ovl_path']

    path = '.:%/Libraries'
    for p in bsv_path_file:
        path += ':'+p

    curr_dir = os.getcwd()

    suppress = ''
    if "all" in foo['bsc_compile_options']['suppress_warnings']:
        suppress += ' -suppress-warnings\
 G0010:T0054:G0020:G0024:G0023:G0096:G0036:G0117:G0015'
    elif "none" not in foo['bsc_compile_options']['suppress_warnings']:
        suppress += ' -suppress-warnings '
        for w in foo['bsc_compile_options']['suppress_warnings']:
            suppress += str(w)+':'
        suppress = suppress[:-1]
    bsc_command = bsc_cmd.format(foo['bsc_compile_options']['verilog_dir'],
            foo['bsc_compile_options']['build_dir'], suppress)

    make_file.write(makefile_temp.format(verilog_dir, bsv_build_dir,
        "bin", bsc_command, bsc_defines,path, bsc_path, top_module, top_dir,
        top_file, top_file[:-4], verilator_cmd,
        verilator_speed, xlen, top_file[:-3]+'bo'))
    if logging:
        logger.info(str(make_file.name)+' generated')

    if logging:
        logger.info('Creating Dependency graph')

    depends = ' BSC_PATH="{0}" BSC_DEFINES="{1}" BSC_BUILDDIR="{2}" \
 BSC_TOPFILE="{3}" OUTPUTFILE=depends.mk ./bluetcl/genDependencies.tcl'.format(\
 path, bsc_defines, bsv_build_dir, top_dir+'/'+top_file)

    os.makedirs(verilog_dir, exist_ok=True)
    os.makedirs(bsv_build_dir, exist_ok=True)
    utils.shellCommand(depends).run(cwd=cwd)
    dependency = open('depends.mk','r').read()
    dependency = dependency.replace('$(BLUESPECDIR)',bsc_path+'/lib')
    newdependency = open('depends.mk','w').write(dependency)
    if logging:
        logger.info('Dependency Graph Created')

def gen_artifacts(uarch_yaml, isa_yaml, debug_yaml, grouping_yaml, logging=False):

    isa_string = isa_yaml['hart0']['ISA']
    if 64 in isa_yaml['hart0']['supported_xlen']:
        xlen = 64
        mabi = 'lp64'
        march = 'rv64i'
    else:
        xlen = 32
        mabi = 'ilp32'
        march = 'rv32i'

    if 'M' in isa_string:
        march += 'm'
    if 'C' in isa_string:
        march += 'c'
    if 'F' in isa_string:
        march += 'F'
    if 'D' in isa_string:
        march += 'D'

    uarch_specific_checks(uarch_yaml, isa_string)
    bsc_defines = gen_bsc_defines(uarch_yaml, isa_yaml['hart0'], debug_yaml,
            grouping_yaml )
    gen_make_include(uarch_yaml, xlen, bsc_defines, logging)

    logger.info('Configuring Boot-Code')
    ofile = open('boot/Makefile.inc','w')
    ofile.write('XLEN='+str(xlen))
    ofile.close()

    logger.info('Configuring the Benchmarks')
    ofile = open('benchmarks/Makefile.inc','w')
    ofile.write('xlen=' + str(xlen) + '\n')
    ofile.write('march=' + march + '\n')
    ofile.write('mabi=' + mabi + '\n')
    ofile.close()

    cwd = os.getcwd()
    if logging:
        logger.info('Cleaning previously built code')
    utils.shellCommand('make clean').run(cwd=cwd)
    if logging:
        logger.info('Run make -j<jobs>')

    genus_synth_script(uarch_yaml, isa_yaml)
