# these are default values. Change only if you know what you are doing
set tech_size {tech_size}
set max_uncert_factor  0.1
set input_delay_factor 0.0
set output_delay_factor 0.0
set loadvalue 0.00806
set enable_clockgating false
set derate_value 1.10

# change below as per your needs
set freq_mhz  {freq_mhz}
set top {top_module}
set verilog_dir {verilog_dir}
set retime_list {{ {retime_list} }}
set clk_port CLK
#set_attr auto_super_thread false
#set_attr super_thread_debug_directory {{./}}
#set_attr super_thread_debug_jobs true

set report_dir rpt
set output_dir output
