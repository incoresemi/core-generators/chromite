# See LICENSE.incore for license details
import logging
import os
import shutil
import sys

from cerberus import Validator
import configure.configure as configure
import configure.utils as utils
from configure.consts import *
import riscv_config.checker as checker
import csrbox.csr_gen as csr_gen
from   csrbox.errors import ValidationError
import csrbox
import riscv_config
from datetime import datetime
import pprint
import textwrap
import time

pp = pprint.PrettyPrinter(indent=2)


def main():
    '''
        Entry point for riscv_config.
    '''

    # Set up the parser
    parser = utils.config_cmdline_args()
    args = parser.parse_args()

    # Set up the logger
    utils.setup_logging(args.verbose)
    logger = logging.getLogger()
    logger.handlers = []
    ch = logging.StreamHandler()
    ch.setFormatter(utils.ColoredFormatter())
    logger.addHandler(ch)

    logger.info('************ Chromite Core Generator ************ ')
    logger.info('------ Copyright (c) InCore Semiconductors ------ ')
    logger.info('---------- Available under BSD License---------- ')
    logger.info('\n\n')

    logger.info('Using CSRBOX Version: '+ str(csrbox.__version__))
    logger.info('Using RISCV-CONFIG Version: '+ str(riscv_config.__version__))

    if args.clean is None:
        update_dep = True
        patch = True
    else:
        update_dep = False
        patch = False

    logger.info('Checking pre-requisites')
    configure.check_prerequisites()

    logger.info('Resolving Repo Dependencies')
    configure.clone_dependencies(args.verbose, args.clean, update_dep, patch)

    if args.clean == True:
        raise SystemExit(0)

    if args.cspec is None:
        logger.error('No CSPEC YAML found')
        raise SystemExit(1)
    
    if args.ispec is None:
        logger.error('No ISA YAML found')
        raise SystemExit(1)

    logger.info(f'Normalizing Core Spec: {args.cspec}')
    uarch_schema = utils.load_yaml('configure/schema.yaml')
    uarch_yaml = utils.load_yaml(args.cspec)
    validator = Validator(uarch_schema)
    validator.allow_unknown = False
    validator.purge_readonly = True
    uarch_normalized = validator.normalized(uarch_yaml, uarch_schema)
    logger.info('Validating Core Spec')
    valid = validator.validate(uarch_normalized)

    # Print out errors
    if valid:
        logger.info('No errors in Core Spec.')
    else:
        error_list = validator.errors
        raise ValidationError(f"Error in {args.cspec}", error_list)

    work_dir= uarch_normalized['bsc_compile_options']['work_dir']
    os.makedirs(work_dir, exist_ok= True)
    
    checked_filename = os.path.split(args.cspec)[1].split('.')[0]+'_checked.yaml'
    checked_file = open(f'{work_dir}/'+checked_filename,'w')
    utils.dump_yaml(uarch_normalized,checked_file,False)
    checked_file.close()

    logger.info(f'Validating ISA YAML: {args.ispec}')
    try:
        isa_file = checker.check_isa_specs(args.ispec, work_dir, True)
    except ValidationError as msg:
        logger.error(msg)
        raise SystemExit(1)

    if args.customspec:
        logger.info(f'Validating CUSTOM YAML: {args.customspec}')
        try:
            custom_file = checker.check_custom_specs(args.customspec, work_dir, True)
        except ValidationError as msg:
            logger.error(msg)
            raise SystemExit(1)
    else:
        logger.info(f'No CUSTOM YAML specified')            
        custom_file = None

    if args.dspec:
        logger.info(f'Validating DEBUG YAML: {args.dspec}')
        try:
            debugfile = checker.check_debug_specs(args.dspec, isa_file, work_dir,
                    True)
        except ValidationError as msg:
            logger.error(msg)
            raise SystemExit(1)
    else:
        logger.info(f'No DEBUG YAML specified')            
        debugfile = None

    isa_normalized = utils.load_yaml(isa_file)
    if args.customspec:
        custom_normalized = utils.load_yaml(custom_file)
    else:
        custom_normalized = {}
    if args.dspec:
        debug_normalized = utils.load_yaml(debugfile)
    else:
        debug_normalized = {}
    grouping_normalized = utils.load_yaml(args.gspec)

    logger.info('Starting CSR generation using CSR-BOX')
    csrbox_dir = 'csrbox/'
    os.makedirs(csrbox_dir, exist_ok= True)
    csr_gen.csr_gen(isa_file, args.gspec, custom_file, debugfile, None, 
                    csrbox_dir, 'soc',logging=True)

    configure.gen_artifacts(uarch_normalized, isa_normalized, debug_normalized,
                                 grouping_normalized, True)

    build_info = open(f'{work_dir}/build.info','w')
    tags = os.popen('git describe --tags --dirty').read()
    commitsha = os.popen('git rev-parse HEAD').read().rstrip()
    dirty = 'Dirty' if 'dirty' in tags else 'Clean'
    myinfo = f'''
Date     : {datetime.now().strftime("%A, %d. %B %Y %I:%M%p")} {time.tzname[0]}
Hostname : {os.uname()[1]}
Sysname  : {os.uname()[0]}
Machine  : {os.uname()[4]}
Branch   : {utils.get_active_branch_name()}
Commit   : {commitsha}
Status   : {dirty}
Args     : 
       ispec = {os.path.abspath(args.ispec)}
       cspec = {os.path.abspath(args.cspec)}
       dspec = {os.path.abspath(args.dspec)}
       gspec = {os.path.abspath(args.gspec)}
       customspec = {os.path.abspath(args.customspec)}
Dependencies: 
       {textwrap.indent(str(dependency_yaml),'        ')}
'''
    build_info.write(myinfo)
    build_info.close()


if __name__ == "__main__":
    exit(main())
