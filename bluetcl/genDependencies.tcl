#! /usr/bin/env bluetcl
#
# Copyright (c) 2020 Alexandre Joannou
# All rights reserved.
#
# This software was developed by SRI International and the University of
# Cambridge Computer Laboratory (Department of Computer Science and
# Technology) under DARPA contract HR0011-18-C-0016 ("ECATS"), as part of the
# DARPA SSITH research programme.
#
# @BERI_LICENSE_HEADER_START@
#
# Licensed to BERI Open Systems C.I.C. (BERI) under one or more contributor
# license agreements.  See the NOTICE file distributed with this work for
# additional information regarding copyright ownership.  BERI licenses this
# file to you under the BERI Hardware-Software License, Version 1.0 (the
# "License"); you may not use this file except in compliance with the
# License.  You may obtain a copy of the License at:
#
#   http://www.beri-open-systems.org/legal/license-1-0.txt
#
# Unless required by applicable law or agreed to in writing, Work distributed
# under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
# CONDITIONS OF ANY KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations under the License.
#
# @BERI_LICENSE_HEADER_END@
#

namespace import ::Bluetcl::*

# there does not seam to be a nice "getopt" in tcl, so for now, falling back on:
#
# - env variables:
# BSC_PATH
# BSC_DEFINES
# BSC_BUILDDIR
# BSC_TOPFILE
# OUTPUTFILE
#
# - if argv is non empty, each arg is passed individually to a flags set command
#   (that is, can't pass flags that expect arguments themselves like -D XXX)
################################################################################

if { [info exists env(BSC_PATH)] } {
  set path $env(BSC_PATH)
} else {
  puts "BSC_PATH environment variable pointing to all paths containing bsv source needs to be set before running this script"
  exit
}

if { [info exists env(BSC_DEFINES)] } {
  set defines [split $env(BSC_DEFINES)]
} else {
  puts "BSC_DEFINES environment variable containing all the compile macros needs to be set before running this script"
  exit
}

if { [info exists env(BSC_BUILDDIR)] } {
  set builddir $env(BSC_BUILDDIR)
} else {
  puts "Defaulting to 'build' as the build directory"
  set builddir "build"
}

if { [info exists env(BSC_TOPFILE)] } {
  set topfile $env(BSC_TOPFILE)
} else {
  puts "BSC_TOPFILE variable not set."
  exit
}

if { [info exists env(OUTPUTFILE)] } {
  set outputfile $env(OUTPUTFILE)
} else {
  set outputfile ".depends.mk"
}
set MYDIR "/scratch/software-files/open-bsc/lib"

# debug prints
puts "path      : $path"
puts "defines   : $defines"
puts "builddir  : $builddir"
puts "topfile   : $topfile"
puts "outputfile: $outputfile"
puts "argv      : $argv"

# setting compiler flags
################################################################################

flags set -p $path
flags set -bdir $builddir
flags set -verilog
foreach i $defines {
  flags set -D $i
}
if { $argc > 0 } { foreach i $argv { flags set $i } }

# output of "depend make" seams to be of the form
# { {tgtA {depA0 depA1 ...}}
#   {tgtB {depB0 depB1 ...}}
#   ...
# }
# reformating it for make
################################################################################
try {
  set res [depend make $topfile]
} finally {
  if { [info exists res] } {
    set fd [open $outputfile w]
    foreach i $res {
      set tgt [lindex $i 0]
      set deps [join [lindex $i 1]]
      puts $fd [append tgt ": " $deps]
    }
    close $fd
    puts "generated make dependency rules for \"$topfile\" in: $outputfile"
  } else {
    puts "could not generate make dependency rules for \"$topfile\""
  }
}
