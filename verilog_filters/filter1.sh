#!/bin/bash
sed -i 's/\$imported_//g' $1

if [[ $1 == *"mkelfmem"* ]]; then
  cat devices/elfmem/elfmem.v  $1 > temp && mv temp $1
fi
